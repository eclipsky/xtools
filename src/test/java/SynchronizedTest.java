public class SynchronizedTest {
}

class Main{
    public static void main(String[] args) {
        SubClass sub = new SubClass();
        for (int i = 0 ;i < 5; i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    sub.say();
                }
            }).start();
        }
    }
}

class SubClass extends ParentClass{

    public synchronized void say(){
        System.out.println(Thread.currentThread().getName() + " -> sub");
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.say();
    }
}

class ParentClass{
    public synchronized void say(){
        System.out.println(Thread.currentThread().getName() + " -> parent");
    }
}
