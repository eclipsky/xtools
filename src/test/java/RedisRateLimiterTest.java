import org.sky.x.util.ratelimiter.RedisRateLimiter;

import java.util.concurrent.CountDownLatch;

public class RedisRateLimiterTest {

    public static void main(String[] args) throws Exception {
        String sha1 = "41508b3d2dc9fe8f781fe185810e2fb194a3aa9d1";
        String key = "ratelimiter:abtest:account";
        int permitsPerSecond = 80;
        RedisRateLimiter rateLimiter = RedisRateLimiter.create(sha1, key, permitsPerSecond);
        System.out.println(System.currentTimeMillis() + " -> get 10 token cost "+ rateLimiter.acquire(10) + "s");
        Thread.sleep(1000);
        System.out.println(System.currentTimeMillis() + " -> get 10 token cost "+ rateLimiter.acquire(10) + "s");
        System.out.println(System.currentTimeMillis() + " -> get 10 token cost "+ rateLimiter.acquire(10) + "s");
        CountDownLatch latch = new CountDownLatch(81);

        for (int i = 0; i < 80; i ++){
            new Thread(() -> {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(System.currentTimeMillis() + " -> get 1 token cost "+ rateLimiter.acquire(1) + "s");
            }).start();
            latch.countDown();
        }
        System.out.println("wait for 1 seconds and acquire permits parallel .....");
        Thread.sleep(1000);
        latch.countDown();
    }

}
