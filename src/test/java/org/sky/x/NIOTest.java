package org.sky.x;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

public class NIOTest {

	public static void readFile() throws Exception {
		RandomAccessFile file = new RandomAccessFile("/tmp/binary.txt", "rw");
		FileChannel channel = file.getChannel();
		ByteBuffer buf = ByteBuffer.allocate(64);
		int readNum = channel.read(buf);
		while (readNum != -1) {
			// flip after write to buffer, make buffer read for reading
			buf.flip();
			while (buf.hasRemaining()) {
				System.out.print((char) buf.get()); // 这里按字节读取，如果有非ASCII字符，将会乱码
			}
			// clear after read from buffer, make buffer read for writing
			buf.clear();
			readNum = channel.read(buf);// Reads a sequence of bytes from this
										// channel into the given buffer.
		}
		// clear(), campact()
		// rewind()
		// mark(), reset()

		file.close();
	}

	public static void scatterAndGather() throws Exception {
		RandomAccessFile file = new RandomAccessFile("d:\\binary.txt", "rw");
		FileChannel channel = file.getChannel();
		ByteBuffer header = ByteBuffer.allocate(128);
		ByteBuffer body = ByteBuffer.allocate(1024);
		// 除非header和body空间固定，不然没卵用
		ByteBuffer[] bufferArray = { header, body };
		channel.read(bufferArray);
		file.close();
	}

	public static void copyFile() throws Exception {
		RandomAccessFile fromFile = new RandomAccessFile("d:\\fromFile.txt",
				"rw");
		FileChannel fromChannel = fromFile.getChannel();

		RandomAccessFile toFile = new RandomAccessFile("d:\\toFile.txt", "rw");
		FileChannel toChannel = toFile.getChannel();

		// fromChannel.transferTo(0, fromChannel.size(), toChannel);
		toChannel.transferFrom(fromChannel, 0, fromChannel.size());
		fromFile.close();
		toFile.close();
	}

	/**
	 * The Channel must be in non-blocking mode to be used with a Selector. This
	 * means that you cannot use FileChannel's with a Selector since
	 * FileChannel's cannot be switched into non-blocking mode. Socket channels
	 * will work fine though.
	 * 
	 * @throws Exception
	 */
	public static void selectorClient() throws Exception {
		SocketChannel channel = SocketChannel.open();
		channel.connect(new InetSocketAddress("http://10.1.2.196", 12345));
		channel.configureBlocking(false);

		Selector selector = Selector.open();
		SelectionKey key = channel.register(selector, SelectionKey.OP_READ);
		while (true) {
			int readyChannels = selector.select();
			if (readyChannels == 0)
				continue;
			Set<SelectionKey> selectedKeys = selector.selectedKeys();
			Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
			while (keyIterator.hasNext()) {
				key = keyIterator.next();
				if (key.isAcceptable()) {
					// a connection was accepted by a ServerSocketChannel.

				} else if (key.isConnectable()) {
					// a connection was established with a remote server.

				} else if (key.isReadable()) {
					channel = (SocketChannel) key.channel();
					ByteBuffer buf = ByteBuffer.allocate(64);
					int num = channel.read(buf);
					while (num > 0) {
						buf.flip();
						while (buf.hasRemaining()) {
							System.out.print(buf.getChar());
						}
						buf.clear();
						num = channel.read(buf);
					}
					// a channel is ready for reading

				} else if (key.isWritable()) {
					// a channel is ready for writing
				}

				keyIterator.remove();
			}
		}
	}

	public static void server() throws IOException {
		ServerSocketChannel channel = ServerSocketChannel.open();
		channel.bind(new InetSocketAddress(12345));
		channel.configureBlocking(false);
		Selector selector = Selector.open();
		int interestOpts = SelectionKey.OP_ACCEPT | SelectionKey.OP_CONNECT | SelectionKey.OP_READ | SelectionKey.OP_WRITE;
		channel.register(selector, interestOpts);
//		channel.register();
		while(selector.select() > 0){
			Set<SelectionKey> keys = selector.selectedKeys();
			for(SelectionKey key: keys){
				key.interestOps();
			}
		}
	}

	public static void client() throws IOException {
		// http://10.1.2.196:12345/dc/proxy/get?num=2&mode=2
		SocketChannel socketChannel = SocketChannel.open();
		socketChannel.connect(new InetSocketAddress("10.1.2.196", 12345));
		ByteBuffer buf = ByteBuffer.allocate(1024);
		int bytesRead = socketChannel.read(buf);
		while (bytesRead > 0) {
			String content = new String(buf.array());
			System.out.println(content);
			buf.clear();
			socketChannel.read(buf);
			
		}
		socketChannel.close();
	}

	public static void main(String[] args) throws Exception {
//		 readFile();
		// copyFile();
		server();
//		client();
		// socketChannel();
	}
}
