package org.sky.x;

import org.iq80.leveldb.DB;
import org.iq80.leveldb.Options;

import java.io.File;
import java.io.IOException;

import static org.iq80.leveldb.impl.Iq80DBFactory.bytes;
import static org.iq80.leveldb.impl.Iq80DBFactory.factory;

public class LevelDBTest {

    public static void main(String[] args) throws IOException {
        DB levelDBStore;
        Options options = new Options();
        levelDBStore = factory.open(new File("levelDBStore"),options);

        levelDBStore.put(bytes("key"),bytes("value"));
        System.out.println(new String(levelDBStore.get(bytes("key"))));
    }
}
