package org.sky.x;

import org.bytedeco.javacv.FrameGrabber;
import org.junit.Test;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class TryCatchTest {

    @Test
    public void test(){
        System.out.println(get());
    }

    public String get(){
        Properties props = new Properties();
        try {
//            props.load(new FileReader("hello.xml"));
            int i = 1/1;
            return "ok";
        } catch (Exception e) {
            System.out.println("in catch");
            // e.printStackTrace();
        }finally {
          System.out.println("in finally");
            return "finally";
        }
    }

}
