package org.sky.x;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTest {
    public static void main(String[] args) {
        ExecutorService exeutor = Executors.newFixedThreadPool(3);
        Integer num = 5;
        Container c = new Container(num);
        Thread1 a = new Thread1("producer", c);
        Thread b = new Thread(new Thread2("consumer", c));
        System.out.println("init num:" + num);
        a.setName("producer");
        b.setName("consumer");
//        a.start();
//        b.start();
        exeutor.submit(a);
        exeutor.submit(b);
        System.out.println("main-thread done");
    }
}

class Container{
    Integer num;
    public Container(int num){
        this.num = num;
    }
}

class Thread1 extends Thread{
    private String threadName;
    private Container data;

    public Thread1(String threadName, Container data){
        this.setName(threadName);
        this.threadName = threadName;
        this.data = data;
    }

    @Override
    public void run() {
        Random random = new Random();
        while (true){
            try {
                synchronized (data){
                    int processNum = random.nextInt(3) + 1;
                    if(data.num + processNum > 20){
                        System.out.println(threadName + ", currentNum: " + data.num + ", processNum: " + processNum + ", too much, waiting...");
                        data.wait();
                    }else {
                        System.out.println(threadName + ", currentNum: " + data.num + ", processNum: " + processNum );
                        data.num = data.num + processNum;
                        Thread.sleep(1000);
                        data.notifyAll();
                    }
                }

            }catch (Exception e){

            }

        }
    }


}

class Thread2 implements Runnable{
    private String threadName;
    private Container data;

    public Thread2(String threadName, Container data){
        this.threadName = threadName;
        this.data = data;
    }

    @Override
    public void run() {
        Random random = new Random();
        while (true){
            try {
                Thread.sleep((int) Math.random() * 1000);
                synchronized (data){
                    int processNum = random.nextInt(3) + 1;
                    if(data.num - processNum < 5){
                        System.out.println(threadName + ", currentNum: " + data.num + ", processNum: " + processNum + ", not enough, waiting...");
                        data.wait();
                    }else {
                        System.out.println(threadName + ", currentNum: " + data.num + ", processNum: " + processNum );
                        data.num = data.num - processNum;
                        Thread.sleep(1000);
                        data.notifyAll();
                    }
                }

            }catch (Exception e){

            }

        }
    }
}

