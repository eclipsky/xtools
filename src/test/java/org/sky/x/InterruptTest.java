package org.sky.x;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class InterruptTest {
	public static void main(String[] args) {
//		Thread t1 = new Thread(() -> {
//            try {
//                Thread.sleep(100000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });
//		t1.start();
//		Thread tm = Thread.currentThread();
//        tm.interrupt();
//		System.out.println(tm.isInterrupted());
//		t1.interrupt();
//		System.out.println(t1.isInterrupted());
		System.out.println("result -> " + new InterruptTest().getResult());
		System.out.println("main over!");
	}

	public String getResult() {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		FutureTask<String> future = new FutureTask<String>(() -> {
			InterruptTest interrupt = new InterruptTest();
			return interrupt.getValue();
		});

		executorService.execute(future);

		String result = null;
		try {
			// 设定超时的时间范围为10秒
			result = future.get(3, TimeUnit.SECONDS);
	 		if(Thread.interrupted()){
	 			throw new InterruptedException("手动中断");
			}
		} catch (InterruptedException e) {
			future.cancel(true);
			System.out.println("方法执行中断");
		} catch (ExecutionException e) {
			future.cancel(true);
			System.out.println("Excution异常");
		} catch (TimeoutException e) {
			future.cancel(true);
			result = "方法执行时间超时";
		}

		executorService.shutdownNow();
		return result;
	}

	public String getValue() {
		try {
			Thread.sleep(5000);
			Thread.currentThread().interrupt();
			System.out.println(Thread.currentThread().isInterrupted());
            System.out.println(Thread.currentThread().isInterrupted());
			System.out.println("正常执行");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "正常结果";
	}
}