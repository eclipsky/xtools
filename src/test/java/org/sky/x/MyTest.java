package org.sky.x;

import org.junit.Test;
import org.sky.x.annotation.LocalVariable;
import org.sky.x.util.ratelimiter.RedisRateLimiter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class MyTest {
	volatile int i = 0;
	@LocalVariable
	int j = 0;

	@Test
	public void getPid(){
		String jvmName = ManagementFactory.getRuntimeMXBean().getName();
		System.out.println(jvmName.split("@")[0]);
	}

//	@Test
	public void intern(){
		String s = "MyBoy";
		String s1 = s.intern();
		System.out.println(s == s1);
		String s2 = new String("MyBoy");
		System.out.println(s == s2);
	}

//	@Test
	public void Oom(){
		List<byte[]> list = new ArrayList<>();
		try{
			while(true){
				byte[] data = new byte[1024 * 1024];
				list.add(data);
				Thread.sleep(2000);
			}
		}catch (Exception e){
			e.printStackTrace();
		}

	}

//	@Test
	public void threadGroup(){
		Thread t1 = new Thread(() -> System.out.println("game over"));
		t1.start();
		SecurityManager sm = System.getSecurityManager();
		sm.checkPrintJobAccess();
	}

//	@Test
	public void lambda(){
        @LocalVariable List<Integer> list = Arrays.asList(1 ,2 ,3);
		list.forEach(i -> System.out.println(i));

		System.out.println("--------");
		list.forEach(System.out::println);

		System.out.println("--------");
		// what happend!
		System.out.println(list.stream().map(i -> i * i).reduce((sum, i) -> sum + i).get());
		System.out.println(list.stream().reduce(10, (a, b) -> a + b));
		System.out.println(list.stream().reduce(0, Integer::sum));
		System.out.println(list.stream().reduce(Integer::sum).get());

		List<String> strList = Arrays.asList("sam", "sara", "jack", "rose");
	}

//	@Test
	public void netTest() throws URISyntaxException, IOException {
		URI uri = new URI("https://www.baidu.com");
		URL url = uri.toURL();
		InputStream in = url.openStream();
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(reader);
		String line = null;
		while ((line = br.readLine()) != null){
			System.out.println(line);
		}
	}

	public void utf8mb4(){
		String str = "铜\uD840\uDC86乡";
		System.out.println(str);
	}

	public void memoryBarrier() {
		i++;
		j++;
		i = j;
	}
}
