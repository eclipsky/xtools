package org.sky.x;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

public class ClassLoaderTest {

	@Test
	public void ResourceLoader(){
		DefaultResourceLoader resourceLoader = new DefaultResourceLoader();
		Resource fakeFileResource = resourceLoader.getResource("D:\\video\\boss.avi");
		assertTrue(fakeFileResource instanceof ClassPathResource);
		assertFalse(fakeFileResource.exists());
		Resource urlResource1 = resourceLoader.getResource("file:D:/video/boss.avi");
		assertTrue(urlResource1 instanceof UrlResource);
		Resource urlResource2 = resourceLoader.getResource("http://www.spring21.cn");
		assertTrue(urlResource2 instanceof UrlResource);
		try{
			fakeFileResource.getFile();
			fail("no such file with path["+fakeFileResource.getFilename()+"] exists in classpath");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try{
			urlResource1.getFile();
		} catch(IOException e){
			e.printStackTrace();
		}
	}

	@Test
	public void myClassLoader() throws Exception {
		ClassLoader myLoader = new ClassLoader(){
			public Class<?> loadClass(String name) throws ClassNotFoundException{
				try{
				System.out.println(name);
				String fileName = name.substring(name.lastIndexOf(".") + 1) + ".class";
				InputStream is = getClass().getResourceAsStream(fileName);
				if(is == null){
					return super.loadClass(name);
				}
				byte[] b = new byte[is.available()];
				is.read(b);
				return defineClass(name, b, 0, b.length);
				}catch(IOException e){
					throw new ClassNotFoundException(name);
				}
			}
		};
		String dupStringClass = "java.lang.String";
		myLoader.loadClass(dupStringClass);
		Object obj = myLoader.loadClass("org.sky.x.ClassLoaderTest").newInstance();
		System.out.println("Class -> " + obj.getClass());
		System.out.println("ClassLoader -> " + obj.getClass().getClassLoader());
		System.out.println("ClassLoader -> " + org.sky.x.ClassLoaderTest.class.getClassLoader());
		System.out.println(obj instanceof org.sky.x.ClassLoaderTest);
		System.out.println("package.access -> " + java.security.Security.getProperty("package.access"));
	}
	
	@Test
	public void getClassLoaderPath() throws Exception{
//		ClassLoader loader = this.getClass().getClassLoader();
//		System.out.println(loader);
//		System.out.println(loader.getResource("HumanTest.class"));
//		System.out.println(System.getProperty("java.class.path"));
		myClassLoader();
	}
	
}