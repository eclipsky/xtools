package org.sky.x;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenericTest {
	public static void main(String[] args) throws Exception {
		// List<Number> list1 = new ArrayList<Integer>(); // ERROR
		List<? extends Number> list2 = new ArrayList<Integer>(); // OK
		System.out.println(list2);
		test2();
	}

	// public static void getData(Box<Number> data) {
	// System.out.println("data:" + data.getData());
	// }

	public static void test2() throws Exception {
		List<Integer> ls = new ArrayList<Integer>();
		ls.add(1);
		List.class.getMethod("add", Object.class).invoke(ls, "b");
		for (int i = 0; i < ls.size(); i++) {
			int item = ls.get(i);
			System.out.println(item);
		}

	}

	public static void test1() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		String key = "key";
		Integer val = new Integer(1);
		// Method m = HashMap.class.getDeclaredMethod("put", new Class[] { Object.class, Object.class });
		Method m = Map.class.getDeclaredMethod("put", new Class[] { Object.class, Object.class });
		m.invoke(map, key, val);
		System.out.println(map); // {key=1}
		System.out.println(map.get(key)); // java.lang.ClassCastException: java.lang.Integer cannot be cast to
											// java.lang.String

	}
}

// class Boy {
// public void test(List<Integer> ls) {
//
// }
// public void test(List<String> ls) {
//
// }
//
// }

class Box<T> {
	private T data;

	public Box() {

	}

	public Box(T data) {
		this.data = data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public <T, T2> T getData1(T newData) {
		return newData;
	}

	public <E> E getData2(E newData) {
		return newData;
	}
}