package org.sky.x;

import org.junit.Test;

import java.net.URL;

public class ClassLoaderDemo {

	@Test
	public void bootstrapClassLoader(){
		System.out.println(System.getProperty("sun.boot.class.path"));
		URL[] urls = sun.misc.Launcher.getBootstrapClassPath().getURLs();
		for (int i = 0; i < urls.length; i++) {
			System.out.println(urls[i].toExternalForm());
		}
	}

	public void getResource() {
		System.out.println("ClassLoaderDemo.class.getClassLoader(): " + ClassLoaderDemo.class.getClassLoader());
		System.out.println("ClassLoaderDemo.class.getClassLoader().getParent(): " + ClassLoaderDemo.class.getClassLoader().getParent());
		System.out.println("ClassLoaderDemo.class.getClassLoader().getParent().getParent(): "
				+ ClassLoaderDemo.class.getClassLoader().getParent().getParent());
		System.out.println("ClassLoaderDemo.class.getResourceAsStream(\"mydata\"): " + ClassLoaderDemo.class.getResourceAsStream("mydata"));
		System.out.println("ClassLoaderDemo.class.getClassLoader().getResourceAsStream(\"mydata\"): "
				+ ClassLoaderDemo.class.getClassLoader().getResourceAsStream("mydata"));

		System.out.println("ClassLoaderDemo.class.getResource(\"mydata\").getPath(): "
				+ ClassLoaderDemo.class.getResource("mydata").getPath());
		System.out.println("ClassLoaderDemo.class.getResource(\"/mydata\").getPath(): "
				+ ClassLoaderDemo.class.getResource("/mydata").getPath());
		System.out.println("ClassLoaderDemo.class.getClassLoader().getResource(\"mydata\").getPath(): "
				+ ClassLoaderDemo.class.getClassLoader().getResource("mydata").getPath());
		System.out.println("ClassLoaderDemo.class.getClassLoader().getResource(\"/mydata\").getPath(): "
				+ ClassLoaderDemo.class.getClassLoader().getResource("/mydata").getPath());
		System.out.println(ClassLoaderDemo.class.getClassLoader());
		System.out.println("1 " + ClassLoaderDemo.class.getResource("").getPath());
		System.out.println("2 " + ClassLoaderDemo.class.getResource("/").getPath());
//		System.out.println("3 " + ClassLoaderDemo.class.getResourceAsStream("/"));
		System.out.println("4 " + ClassLoaderDemo.class.getClassLoader().getResource("").getPath());
		System.out.println("5 " + ClassLoaderDemo.class.getClassLoader().getResource("/"));
		System.out.println("6 " + ClassLoaderDemo.class.getClassLoader().getResourceAsStream(""));
	}
}
