package org.sky.x.listener;

import javax.annotation.Resource;

@Resource
public class SimpleMethodExecutionEventListener implements MethodExecutionEventListener {
    @Override
    public void onMethodBegin(MethodExecutionEvent evt) {
        System.out.println("begin execetion: " + evt.getMethodName());
    }

    @Override
    public void onMethodEnd(MethodExecutionEvent evt) {
        System.out.println("end execetion: " + evt.getMethodName());
    }
}
