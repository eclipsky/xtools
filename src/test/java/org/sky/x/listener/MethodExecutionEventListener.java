package org.sky.x.listener;

import java.util.EventListener;

public interface MethodExecutionEventListener extends EventListener {
     void onMethodBegin(MethodExecutionEvent evt);
     void onMethodEnd(MethodExecutionEvent evt);
}
