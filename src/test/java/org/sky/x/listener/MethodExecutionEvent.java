package org.sky.x.listener;

import java.util.EventObject;

public class MethodExecutionEvent extends EventObject {
    private String methodName;

    public MethodExecutionEvent(Object source, String methodName){
        super(source);
        this.methodName = methodName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public static enum Status{
        BEGIN("begin"),
        END("end");
        String flag;
        Status(String flag){
            this.flag = flag;
        }
    }
}


