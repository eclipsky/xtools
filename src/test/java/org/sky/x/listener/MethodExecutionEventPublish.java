package org.sky.x.listener;

import java.util.ArrayList;
import java.util.List;

import org.sky.x.listener.MethodExecutionEvent.Status;

public class MethodExecutionEventPublish {
    private List<MethodExecutionEventListener> listenerList = new ArrayList<MethodExecutionEventListener>();

    public void pulishEvent(Status status, MethodExecutionEvent event){
        List<MethodExecutionEventListener> copyListeners = new ArrayList<>(listenerList);
        for(MethodExecutionEventListener listener:copyListeners){
            if(status == Status.BEGIN){
                listener.onMethodBegin(event);
            }else{
                listener.onMethodEnd(event);
            }
        }
    }

    public void addListener(MethodExecutionEventListener listener){
        listenerList.add(listener);
    }

    public void removeListner(MethodExecutionEventListener listener){
        listenerList.remove(listener);
    }

    public void clearAllListner(){
        listenerList.clear();
    }

    public static void main(String[] args) {

    }
}
