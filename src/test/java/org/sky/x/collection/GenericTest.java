package org.sky.x.collection;

import java.util.ArrayList;
import java.util.List;

import com.sun.javafx.font.t2k.T2KFactory;

import sun.font.Type1Font;

/**
 * 泛型测试
 * 
 * @author sam
 *
 */
public class GenericTest {
	public static void main(String[] args) {
		String[] a1 = { "abc" };
		Object[] a2 = a1;
		a2[0] = new Integer(17); // 
		String s = a1[0]; // ArrayStore Exception
//		test();
	}

	public static void test() {
		Number[] numArray = { 1, 2, 3 };
		System.out.println(numArray);
		Integer[] intArray = new Integer[] { 1, 2, 3 };
		Object[] objArray = intArray;
		objArray[0] = new String("sam");
		Integer i = intArray[0];
		numArray = intArray;
		System.out.println(numArray);

		List<? extends Number> numList = new ArrayList<Number>();
		List<Integer> intList = new ArrayList<Integer>();
		numList = intList;
		
		GenericClass genericClass = new GenericClass();
		genericClass.getData("123", "123");
		genericClass.getNumber(123);
	}

}

class GenericClass {
	public <T1, T2> void getData(T1 t1, T2 t2) {
		System.out.println(t1.equals(t2));
	}
	
	public <T extends Number> T getNumber(T t){
		System.out.println(t);
		return t;
	}
}
