package org.sky.x.collection;

import java.util.Collection;

public interface MyQueue<E> extends Collection<E>{
	boolean add(E e);

}
