package org.sky.x;

public class Singleton {
	
	public static Singleton singleton = new Singleton();
	public static int a;
	public static int b = 0;
	private String hello;
	
	private Singleton() {
		super();
		a++;
		b++;
	}

	public static Singleton getInstence() {
		return singleton;
	}

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		Singleton mysingleton = Singleton.getInstence();
		System.out.println(mysingleton.a);
//		System.out.println(mysingleton.b);
	}

	public String getHello() {
		return hello;
	}

	public void setHello(String hello) {
		this.hello = hello;
	}

}
