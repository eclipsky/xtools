package org.sky.x.scala

import java.util.{Date, Locale}
import java.text.DateFormat._

object FrenchDate {
  def main(args: Array[String]): Unit = {
    val now = new Date
    val df = getDateInstance(FULL, Locale.FRENCH)
    println(df format now)
  }
}
