package org.sky.x.scala

import java.net.{ServerSocket, Socket}
import java.nio.ByteBuffer

object ServerTest{
  def main(args: Array[String]): Unit = {
    System.out.println("server start...")
    val server = new ServerSocket(80)
    while(true){
      val socket = server.accept();
      val in = socket.getInputStream()
      val out = socket.getOutputStream()
      var buffer = new Array[Byte](1024)
      in.read(buffer)
      System.out.println(new String(buffer))
      buffer = "welcome, i'm server".getBytes;
      out.write(buffer);
    }
  }
}

object ClientTest{
  def main(args: Array[String]): Unit = {
    System.out.println("client start...");
    val socket = new Socket("localhost", 80);
    val in = socket.getInputStream();
    val out = socket.getOutputStream();
    val msg = "hello, i'm sam"
    out.write(msg.getBytes);
    out.flush();
    val buffer = new Array[Byte](1024);
    in.read(buffer);
    System.out.println(new String(buffer));
  }
}
