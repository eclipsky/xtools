//package org.sky.x.scala;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.Random;
//
//object SparkJdbcClient {
//    val driverName = "org.apache.hive.jdbc.HiveDriver";
//    val jdbcUrl = "jdbc:hive2://localhost:9990/default";
//
//    /**
//     * @param args
//     * @throws SQLException
//     */
//    def main(args: Array[String]) {
//        try {
//            Class.forName(driverName);
//        } catch (ClassNotFoundException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//            System.exit(1);
//        }
//        singleThread();
//    }
//
//    def singleThread():Unit {
//        val t1 = new Thread(new SparkClient("iqltest@sinaif.com", "admin123"), "iqltest");
//        t1.start();
//    }
//
//    private static void multiThread() {
//        Random rand = new Random();
//        int seed = rand.nextInt(10);
//        int i = 1;
//        while (true) {
//            int no = 10000 + i;
//            Thread t1 = new Thread(new SparkClient("user_" + no, "pass_" + no), "user_" + no);
//            t1.start();
//            try {
//                Thread.sleep(seed * 1000);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            i++;
//        }
//
//    }
//
//    static class SparkClient implements Runnable {
//
//        private String user;
//        private String password;
//        private Connection conn;
//        private Statement stmt;
//
//        public SparkClient(String user, String password) {
//            this.user = user;
//            this.password = password;
//            try {
//                conn = DriverManager.getConnection(jdbcUrl, user, password);
////                conn.getSchema();
//                stmt = conn.createStatement();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//
//        @Override
//        public void run() {
//            executeQuery("select current_database()");
////            executeQuery("show databases");
//            executeQuery("select * from sinaif.t_user_account");
////            execute("drop table if exists sinaif_tmp.mytable");
//            execute("create table if not exists " + "sinaif_test.mytable" + " (key int, value string)");
//            execute("insert into sinaif.mytable" + " values (1, 'sam'),(2, 'eva')");
//            execute("update cx_sinaif_king_dm.phone_sales_group set userid = 100");
//        }
//
//        private void execute(String sql) {
//            try {
//                System.out.println("execting sql >>>>>>>>>> " + sql);
//                stmt.execute(sql);
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//
//        private void executeQuery(String sql) {
//            try {
//                System.out.println("execting sql >>>>>>>>>> " + sql);
//                ResultSet res = stmt.executeQuery(sql);
//                while (res.next()) {
//                    System.out.println(res.getString(1));
////                    System.out.println(String.valueOf(res.getInt(1)) + "\t" + res.getString(2));
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//}
