package org.sky.x.scala
//
//object Hello {
//  val a1 = "sam_o"
//}
//
//class Hello(id: Int){
//  val a1 = "sam_c"
//  def say(name: String) {
//    println(s"id=$id, name=$name")
//  }
//}
//
//object HelloTest{
//  def main(args: Array[String]): Unit = {
//    println(Hello.a1)
//    val hello = new Hello(18)
//    println(hello.a1)
//    hello.say("scala")
//  }
//}
object HelloWorld{
  def main(args: Array[String])= {
    println("Hello World")
  }
}

object Main{
  def main(args: Array[String]): Unit = {
    println("hello, scala")
    val point = new Point
    println(s"${point.x1}, ${point.y}")
    point.x1 = 10
    point.y = 11
    println(s"${point.x1}, ${point.y}")
  }
}

class Point{
  private var _x = 0
  private var _y = 0
  private val bound = 100
  def x1 = _x
  def x1_=(newValue: Int): Unit ={
    if(newValue < bound) _x = newValue else printWarning
  }
  def y = _y
  def y_=(newValue: Int): Unit ={
    if(newValue < bound) _y = newValue else printWarning
  }
  private def printWarning = println("WARNING: Out of bounds")
}

trait HairColor

trait Iterator[A]{
  def hasNext: Boolean
  def next(): A
}

class IntIterator(to: Int) extends Iterator[Int]{
  private var current = 0;
  override def hasNext: Boolean = current < to
  override def next(): Int = {
    if(hasNext){
      val t = current;
      current += 1
      t
    }else 0
  }
}

import scala.util.Random
object CustomerID{
  def apply(name: String) = s"$name--${Random.nextLong}"
}

abstract class Animal {
  def name: String
}
case class Cat(name: String) extends Animal
case class Dog(name: String) extends Animal

object CovarianceTest extends App {
  def printAnimalNames(animals: List[Animal]): Unit = {
    animals.foreach { animal =>
      println(animal.name)
    }
  }

  val cats: List[Cat] = List(Cat("Whiskers"), Cat("Tom"))
  val dogs: List[Dog] = List(Dog("Fido"), Dog("Rex"))

  printAnimalNames(cats)
  // Whiskers
  // Tom

  printAnimalNames(dogs)
  // Fido
  // Rex
}