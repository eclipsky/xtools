package org.sky.x.scala

class Person(name: String, age: Int) {
  def info():Unit = println(name + "," + age);
}

object Person{
  def main(args: Array[String]): Unit = {
    val p = new Person("sam", 18)
    p.info()
  }
}


