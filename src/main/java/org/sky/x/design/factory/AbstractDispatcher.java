package org.sky.x.design.factory;

public abstract class AbstractDispatcher {

	public abstract void doDispatcher();
	
}
