package org.sky.x.design.decorator.demo1;

public class Pants extends AbstractClothes {
    public Pants(AbstractHuman human) {
        super(human);
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub
        super.show();
        showPants();
    }

    public void showPants() {
        System.out.println("get pants");
    }
}
