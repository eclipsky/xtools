package org.sky.x.design.decorator.demo1;

public class Human implements AbstractHuman {
    public String name;
    
    public Human(String name){
        this.name = name;
    }
    
    public void show() {
        System.out.println(name + " is born and naked");
    }
}
