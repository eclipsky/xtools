package org.sky.x.design.decorator.demo1;

public class Tshirt extends AbstractClothes {
    
    public Tshirt(AbstractHuman human) {
        super(human);
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub
        super.show();
        showTshirt();
    }

    public void showTshirt() {
        System.out.println("get tshirt");
    }
}
