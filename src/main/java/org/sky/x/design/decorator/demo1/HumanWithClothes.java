package org.sky.x.design.decorator.demo1;

public class HumanWithClothes {
    public static void main(String[] args) {
        // AbstractHuman human = new Human("jack");
        // human = new Hat(human);
        // human = new Pants(human);
        // human = new Tshirt(human);
        // human.show();
        AbstractHuman human = new Tshirt(new Pants(new Hat(new Human("jack"))));
        human.show();
    }
}
