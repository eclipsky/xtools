package org.sky.x.design.decorator.demo1;

public class Hat extends AbstractClothes {
    public Hat(AbstractHuman human) {
        super(human);
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub
        super.show();
        showHat();
    }

    public void showHat() {
        System.out.println("get hat");
    }
}
