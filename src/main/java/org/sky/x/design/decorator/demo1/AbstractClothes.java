package org.sky.x.design.decorator.demo1;

public abstract class AbstractClothes implements AbstractHuman {
    public AbstractHuman human;

    public AbstractClothes(AbstractHuman human) {
        this.human = human;
    }

    public void show() {
        human.show();
    }
}
