package org.sky.x.design.adapter;

public interface TargetInterface extends CommentInterface {

	public abstract void speak();
	
}
