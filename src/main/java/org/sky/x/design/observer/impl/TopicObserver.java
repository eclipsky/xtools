package org.sky.x.design.observer.impl;

import org.sky.x.design.observer.Observable;
import org.sky.x.design.observer.Observer;

public class TopicObserver implements Observer {

	public String name;

	public TopicObserver(String name, Observable o) {
		this.name = name;
		o.addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		System.out.println(name + " is noticed, topic has changed:" + arg);
	}

}
