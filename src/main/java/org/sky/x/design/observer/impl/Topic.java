package org.sky.x.design.observer.impl;

import org.sky.x.design.observer.Observable;

public class Topic extends Observable {

	private int data;

	public void setData(int i) {
		data = i;
		setChanged();
		notifyObservers(data);
	}

	public int getData() {
		return data;
	}
	
	
}
