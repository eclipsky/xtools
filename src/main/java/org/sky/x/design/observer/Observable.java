package org.sky.x.design.observer;

import java.util.ArrayList;
import java.util.List;

public class Observable {

	private boolean changed = false;

	private List<Observer> obs;

	public Observable() {
		obs = new ArrayList<Observer>();
	}

	public synchronized void addObserver(Observer o) {
		if (!obs.contains(o)) {
			obs.add(o);
		}
	}

	public synchronized void delObserver(Observer o) {
		obs.remove(o);
	}

	public void notifyObservers() {
		notifyObservers(null);
	}

	public synchronized void notifyObservers(Object arg) {
		if (!changed) {
			return;
		}
		changed = false;
		for (Observer o : obs) {
			o.update(this, arg);
		}
	}

	protected void setChanged() {
		changed = true;
	}
}
