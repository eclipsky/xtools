package org.sky.x.design.observer;

public interface Observer {

	public void update(Observable o, Object arg);
}
