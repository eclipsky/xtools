package org.sky.x.design.proxy;

public interface GirlInfo {

	public abstract void hasBoyFriend();
	
	public abstract void BoyFriendSmart();
	
}
