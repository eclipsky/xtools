package org.sky.x.design.listener;

import java.util.Enumeration;
import java.util.EventListener;
import java.util.Vector;

public class MyEventSource {

    private Vector<EventListener> allListeners = new Vector<EventListener>();

    public MyEventSource() {

    }

    public void addEventListener(MyEventListener listener) {
        allListeners.addElement(listener);
    }

    public void notifyEventListener() {
        Enumeration<EventListener> listeners = allListeners.elements();
        while (listeners.hasMoreElements()) {
            MyEventListener listener = (MyEventListener) listeners.nextElement();
            listener.handleEvent(new MyEventObject(this));
        }
    }

    public static void main(String[] args) {
        MyEventSource source = new MyEventSource();
        MyEventListener applicationEventListener = new MyEventListener() {
            public void handleEvent(MyEventObject event) {
                event.say();
                System.out.println("i don't know how to handle it, go away");
            }
        };
        source.addEventListener(applicationEventListener);
        source.notifyEventListener();
    }
}
