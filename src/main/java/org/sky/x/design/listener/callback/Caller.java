package org.sky.x.design.listener.callback;

public class Caller {
    public void call(ICallBack callBack) {
        System.out.println("call begin...");
        callBack.callBack();
        System.out.println("call end...");
    }

    public static void main(String[] args) {
        Caller call = new Caller();
        call.call(new ICallBack() {
            @Override
            public void callBack() {
                System.out.println("调用了一个匿名内部类");
            }
        });
    }
}
