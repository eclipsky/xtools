package org.sky.x.design.listener;

import java.util.EventListener;

/**
 * 事件监听器
 * @author sam.xie
 * @date Jan 9, 2018 5:40:23 PM
 * @version 1.0
 */
public interface MyEventListener extends EventListener{

	public void handleEvent(MyEventObject eventObject);
	
}
