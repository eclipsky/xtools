package org.sky.x.design.listener;

import java.util.EventObject;

/**
 * 事件对象（该事件对象包装了事件源对象、作为参数传递给监听器、很薄的一层包装类）
 * 
 */
public class MyEventObject extends EventObject {

    public MyEventObject(Object source) {
        super(source);
    }

    public void say() {
        System.out.println("hi, listener! something happend!");
    }

    private static final long serialVersionUID = 1L;
}
