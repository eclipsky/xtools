package org.sky.x.design.facade.impl;

import org.sky.x.design.facade.ClothingStore;

public class ClothingStoreImpl implements ClothingStore {

	public void getClothing() {
        System.out.println("this is the ClothingStoreImpl.getClothing()");
	}

}
