package org.sky.x.design.facade;

public interface ClothingStore {
	
	public abstract void getClothing();

}
