package org.sky.x.design.facade.impl;

import org.sky.x.design.facade.EleStore;

public class EleStoreImpl implements EleStore {

	public void getEle() {
		System.out.println("this is the EleStoreImpl.getEle()");
	}

}
