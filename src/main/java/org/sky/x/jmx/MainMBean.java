package org.sky.x.jmx;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.rmi.registry.LocateRegistry;
import java.util.Properties;

public class MainMBean {

    public static void main(String[] args)
            throws Exception {

        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = new ObjectName("org.sky.x:type=hello,name=hello1");
//        ObjectName name = new ObjectName("helloMBean:name=hello");
        Hello mbean = new Hello();
        mbs.registerMBean(mbean, name);
//        mbs.getMBeanInfo("");
        LocateRegistry.createRegistry(9999);
        JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:9999/jmxrmi");
        JMXConnectorServer jcs = JMXConnectorServerFactory.newJMXConnectorServer(url, null, mbs);
        jcs.start();
        systemMBeans();
        System.out.println("Waiting forever...");
        Thread.sleep(Long.MAX_VALUE);
    }

    public static void systemMBeans(){
        OperatingSystemMXBean osMBean = ManagementFactory.getOperatingSystemMXBean();
        RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();

    }
}
