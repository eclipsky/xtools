package org.sky.x.jmx;


public interface QueueSamplerMBean {
    public QueueSample getQueueSample();
    public void clearQueue();
}
