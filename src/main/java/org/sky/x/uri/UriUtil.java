package org.sky.x.uri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class UriUtil {
    public static void main(String[] args) throws Exception {
        demo();
    }

    public static void demo() throws URISyntaxException, IOException {
        URI uri = new URI("https://www.baidu.com");
        URL url = uri.toURL();
        InputStream in = url.openStream();
        InputStreamReader reader = new InputStreamReader(in);
        BufferedReader br = new BufferedReader(reader);
        String line;
        while ((line = br.readLine()) != null){
            System.out.println(line);
        }
    }
}
