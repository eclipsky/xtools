package org.sky.x.algorithm.recursion;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * <pre/>
 * 获取一个单词的所有变形词
 * 比如cat，可以变形为act,atc,cat,cta,tac,tca
 * 如果字母有重复该如何处理？
 * 
 * <pre/>
 * @author sam.xie
 *
 */
public class Anagram {

	public String word = "cats";

	@Test
	public void test() {
		String[] data = doAnagram(word);
		int total = 3 * 2;
		int i = 0;
		for (String item : data) {
			System.out.print(item);
			i++;
			if (i % total == 0) {
				System.out.print("\n");
			} else {
				System.out.print("\t");
			}
		}
	}

	public String[] doAnagram(String subWord) {
		int size = subWord.length();
		if (size == 2) {
			return new String[] {
					subWord,
					String.valueOf(new char[] { subWord.charAt(1),
							subWord.charAt(0) }) };
		} else {
			List<String> result = new ArrayList<String>();
			for (int i = 0; i < size; i++) {
				char firstChar = subWord.charAt(0);
				String newWord = subWord.substring(1);
				String[] words = doAnagram(newWord);
				for (String word : words) {
					result.add(String.valueOf(firstChar) + word);
				}
				// 轮转
				subWord = rotate(subWord);
			}
			return result.toArray(new String[0]);
		}
	}

	/**
	 * <pre/>
	 * 字符串左移一位，第一个字符补全在最右位置上
	 * 例如	myhat -> yhatm	
	 *      myhat -> myath
	 * @param len
	 */
	private String rotate(String subWord) {
		return subWord.substring(1) + subWord.substring(0, 1);
	}

	public static void main(String[] args) {
		Integer a = 10;
		System.out.println(Integer.toHexString(a));
	}

}
