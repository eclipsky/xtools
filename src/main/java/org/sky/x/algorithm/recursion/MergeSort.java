package org.sky.x.algorithm.recursion;

import org.junit.Before;
import org.junit.Test;
import org.sky.x.algorithm.ArrayUtil;

/**
 * <pre/>
 * 归并排序：递归实现
 * 
 * @author sam.xie
 *
 */
public class MergeSort {

	int[] rawData;
	int[] sortedData;

	@Before
	public void prepare() {
		int length = 10;
		rawData = ArrayUtil.getRandomArray(length);
		sortedData = new int[length];
	}

	@Test
	public void test() {
		ArrayUtil.formatArray(rawData);
		mergeSort(0, rawData.length - 1);
		ArrayUtil.formatArray(sortedData);
	}

	public void mergeSort(int left, int right) {
		if (left == right) {
			return;
		}
		int middle = (left + right) / 2; // 0, 1, 2, 3, 4
		mergeSort(left, middle);
		mergeSort(middle + 1, right);
		merge(left, middle + 1, right);
	}

	public void merge(int left, int middle, int right) {
		// 12 15 32, 11 14
		int leftFix = left;
		int sortedIdx = left;
		int leftEnd = middle - 1;
		while (left <= leftEnd && middle <= right) {
			if (rawData[left] <= rawData[middle]) {
				sortedData[sortedIdx++] = rawData[left++];
			} else {
				sortedData[sortedIdx++] = rawData[middle++];
			}
		}
		while (middle <= right) {// 左侧数组已经处理完，剩下只用处理右侧数组
			sortedData[sortedIdx++] = rawData[middle++];
		}
		while (left <= leftEnd) {// 右侧数组已经处理完，剩下只用处理左侧数组
			sortedData[sortedIdx++] = rawData[left++];
		}

		while (leftFix <= right) {
			rawData[leftFix] = sortedData[leftFix];
			leftFix++;
		}
		// ArrayUtil.formatArray(rawData);
	}
}
