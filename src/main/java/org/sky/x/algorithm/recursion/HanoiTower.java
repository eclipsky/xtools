package org.sky.x.algorithm.recursion;

import org.junit.Test;

/**
 * <pre/>
 *   -
 *  ---    
 * -----
 * A		B		C
 * 
 * 汉诺塔问题，A柱从上到下，盘子递增，借助B住，把A柱的盘子转移到C柱上，不允许大盘子在小盘子之上
 * @author sam.xie
 *
 */
public class HanoiTower {

	@Test
	public void test() {
		int num = 3;
		move(num, 'A', 'B', 'C');
	}

	private void move(int topN, char from, char buffer, char to) {
		if (topN == 1) {
			System.out.println("disk " + topN + " move from " + from + " to "
					+ to);
		} else {
			move(topN - 1, from, to, buffer);
			System.out.println("disk " + topN + " move from " + from + " to "
					+ to);
			move(topN - 1, buffer, from, to);
		}
	}
}
