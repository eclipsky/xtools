package org.sky.x.algorithm.recursion;

/**
 * <pre/>
 * 毕达哥拉斯三角
 * 1	3	6	10	15	21
 * 
 * 递归就是程序设计中的数学归纳法：
 * tri(n) = 1	if n = 1
 * tri(n) = n + tri(n-1)	if n > 1
 * 
 * 采用递归是从概念上简化了问题，而不是本质上更高效
 * 
 */
public class TriangleSum {
	public static void main(String[] args) {
		int num = 5;
		System.out.println(sumByLoop(num));
		System.out.println(sumByRecursion(num));
	}

	public static int sumByLoop(int n) {
		int sum = 0;
		for (int i = 1; i <= n; i++) {
			sum += i;
		}
		return sum;
	}

	public static int sumByRecursion(int n) {
		System.out.println("enter -> " + n);
		int sum;
		if (n == 1) {
			sum = 1;
		} else {
			sum = sumByRecursion(n - 1) + n;
		}
		System.out.println("return -> " + n);
		return sum;
	}
}
