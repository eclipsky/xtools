package org.sky.x.algorithm.md5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MD5 {

	// MD5签名算法
	public static String MD5Encode(String str) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte[] encodedPassword = md.digest();
			java.lang.StringBuilder sb = new java.lang.StringBuilder();
			for (int i = 0; i < encodedPassword.length; i++) {
				if ((encodedPassword[i] & 0xff) < 0x10) {
					sb.append("0");
				}
				sb.append(Long.toString((encodedPassword[i] & 0xff), 16));
			}
			return (sb.toString().toUpperCase());
		} catch (Exception ex) {
		}
		return null;
	}


	// 将请求数据流转成字符串
	public static String inputStream2String(InputStream in) throws IOException {
		StringBuffer out = new StringBuffer();
		byte[] b = new byte[4096];
		for (int n; (n = in.read(b)) != -1;) {
			out.append(new String(b, 0, n));
		}
		return out.toString();
	}

	public static void main(String[] args) {
		String proclaime1 = "menglei";
		String secret1 =  MD5.MD5Encode(proclaime1);
		System.out.println("明文proclaime1：="+proclaime1 +" ,密文secret1="+secret1);
		
		String secret2 =  MD5.MD5Encode(secret1);
		System.out.println("明文secret1：="+secret1 +" ,密文secret2="+secret2);
	}
}
