package org.sky.x.algorithm.math;

public class Sqrt {

	/**
	 * <pre/>
	 * 牛顿开平方，res=1;res=(res+x/res)/2; 
	 * @author sam.xie
	 * @date Apr 26, 2019 4:49:49 PM
	 * @param num
	 * @param precision
	 * @return
	 */
	public static double sqrt(int num, int precisionCount) {
		double res = 1;
		double precision = 1;
		while(precisionCount>0) {
			precision/=10;
			precisionCount--;
		}
		while(Math.abs(res * res - num) > precision){
			res = (res + num/res)/2;
		}
		return res;
	}
	
	public static void main(String[] args) {
		int num = 10;
		System.out.println(sqrt(num, 10));
		System.out.println(Math.sqrt(num));
	}
}
