package org.sky.x.algorithm.sort;

import org.sky.x.algorithm.ArrayUtil;

/**
 * <pre/>
 * 简单排序：冒泡排序，选择排序，插入排序
 * 			1、时间复杂度通常为O(N*N)
 * 			2、所有排序算法都只需要一个临时变量
 * 			3、其中冒泡，插入算法稳定，选择不稳定
 * 中级排序：归并排序
 * 			1、时间复杂度O(N*logN)
 * 高级排序：希尔排序，快速排序，基数排序，堆排序
 * 
 * https://zhuanlan.zhihu.com/p/52884590
 * https://www.bilibili.com/video/av685670/
 * 
 * 概念说明：
 * 1、相同关键字的数据项，经过排序它们的顺序保持不变，这样的排序就是稳定的。
 * 2、
 * @author sam.xie
 *
 */
public class MethodsOfSort {

	public static int[] unSortedArray;

	public static final int LENGTH = 10;

	public static void main(String[] args) {
		unSortedArray = ArrayUtil.getRandomArray(LENGTH);
		formatArray(unSortedArray);
		System.out
				.println("--------------------------------------------------");
		// int[] sortedArray = bubbleSort();
		// int[] sortedArray = choiceSort();
		 int[] sortedArray = insertSort();
		// int[] sortedArray = mergeSort();
//		int[] sortedArray = shellSort();
		// int[] sortedArray = heapSort();
		// int[] sortedArray = quickSort();
		// int[] sortedArray = radixSort();
		System.out
				.println("--------------------------------------------------");
		formatArray(sortedArray);
	}

	/**
	 * <pre/>
	 * 冒泡排序，可以算是最直观（笨拙）的排序，相邻两个进行比较，按左小右大的原则交换位置，就像气泡一样最大的数据浮出
	 * 比较次数：N*N/2
	 * 交换次数：N*N/4 (假设数据完全随机)
	 * 时间复杂度：O(N*N)
	 * 
	 * @return
	 */
	public static int[] bubbleSort() {
		int a[] = ArrayUtil.copyArray(unSortedArray);
		for (int i = 0; i < a.length - 1; i++) {
			for (int j = 0; j < a.length - i - 1; j++) {
				int left = a[j];
				int right = a[j + 1];
				if (left > right) {
					swap(a, j, j + 1);
				}
			}
			formatArray(a);
		}
		return a;
	}

	/**
	 * <pre/>
	 * 选择排序，改进的冒泡排序，每次循环获取最小值并使用临时变量保存索引，循环结束后交换
	 * 比较次数：N*N/2
	 * 交换次数：N/2
	 * 时间复杂度：O(N*N)
	 * 
	 * @return
	 */
	public static int[] choiceSort() {
		int[] a = ArrayUtil.copyArray(unSortedArray);
		int min;
		for (int i = 0; i < a.length; i++) {
			min = i;
			for (int j = i + 1; j < a.length; j++) {
				if (a[min] > a[j]) {
					min = j;
				}
			}
			if (i != min) {
				swap(a, i, min);
			}
			formatArray(a);
		}
		return a;
	}

	/**
	 * <pre/>
	 * 插入排序：保证左边局部有序，右边的数依次插入左边有序队列中
	 * 时间复杂度：O(N*N)，对于已经有序或基本有序的数据来说，插入排序只需要O(N)的时间
	 * [2,5,6,4,9,7,6]
	 * @return
	 */
	public static int[] insertSort() {
		int[] a = ArrayUtil.copyArray(unSortedArray);
		int length = a.length;
		for (int i = 1; i < length; i++) {
			int tmp = a[i];
			int j = i - 1;
			while (j >= 0 && a[j] > tmp) {
				a[j+1] = a[j];
				j--;
			}
			a[j+1] = tmp;
			formatArray(a);
		}
		return a;
	}

	/**
	 * <pre/>
	 * 归并排序
	 * 
	 * @return
	 */
	public static int[] mergeSort() {
		// @see MergeSort
		return null;
	}

	/**
	 * 希尔排序
	 * 
	 * @return
	 */
	public static int[] shellSort() {
		int[] a = ArrayUtil.copyArray(unSortedArray);
		int d = a.length;
		return a;
	}

	/**
	 * <pre/>
	 * 堆排序
	 * 
	 * @return
	 */
	public static int[] heapSort() {
		int a[] = ArrayUtil.copyArray(unSortedArray);
		int d = a.length;
		return a;
	}

	/**
	 * <pre/>
	 * 快速排序（也叫二分排序）
	 * 
	 * @return
	 */
	public static int[] quickSort() {
		int a[] = ArrayUtil.copyArray(unSortedArray);
		int d = a.length;
		_quickSort(a, 0, d - 1);
		return a;
	}

	/**
	 * <pre/>
	 * 基数排序
	 * @return
	 */
	public static int[] radixSort() {
		int a[] = ArrayUtil.copyArray(unSortedArray);
		int d = a.length;
		return a;
	}

	/**
	 * 
	 */
	private static void buildMaxHeap(int[] data, int lastIndex) {
		int k = (lastIndex - 1) / 2;// 
		for (int i = k; i >= 0; i--) {
			// 
			int subNode;
			while (i * 2 + 1 <= lastIndex) {// 
				subNode = 2 * i + 1;
				if (subNode + 1 <= lastIndex
						&& data[subNode] < data[subNode + 1]) {// 
					subNode++;
				}
				if (data[i] < data[subNode]) {// 
					swap(data, i, subNode);
					i = subNode;
				} else {
					break;
				}
			}
		}
	}

	/**
	 * 
	 * @param a
	 * @param low
	 * @param high
	 */
	private static void _quickSort(int[] a, int low, int high) {
		// TODO Auto-generated method stub
		int middle;
		if (low < high) {
			middle = getMiddle(a, low, high);
			_quickSort(a, low, middle - 1);
			_quickSort(a, middle + 1, high);
		}
	}

	/**
	 * 
	 * 
	 * @param list
	 * @param low
	 * @param high
	 * @return
	 */

	private static int getMiddle(int[] a, int low, int high) {
		int tmp = a[low];
		while (low < high) {
			while (low < high && tmp <= a[high]) {
				high--;
			}
			a[low] = a[high];
			while (low < high && a[low] <= tmp) {
				low++;
			}
			a[high] = a[low];
		}
		a[low] = tmp;
		return low;
	}

	/**
	 * 交换数组的两个索引值
	 * 
	 * @param data
	 * @param i
	 * @param j
	 */
	private static void swap(int[] data, int i, int j) {
		int tmp = data[i];
		data[i] = data[j];
		data[j] = tmp;
	}

	private static void formatArray(int[] data) {
		for (int i = 0; i < LENGTH; i++) {
			System.out.print(data[i] + "\t");
		}
		System.out.println();
	}

	private static void newline() {
		System.out.println();
	}

	private static void print(String content) {
		System.out.println(content);
	}

}