package org.sky.x.algorithm.tree;

import org.junit.Test;

public class BinaryTree {

	// int total = 0;
	//
	// @Test
	public void sumTreeNodes() {
		Node root = generateTree();
		int result = add(root);
		System.out.println(result);
	}

	@Test
	public void matixTransfer() {
		int[][] arr1 = { { 1, 2, 3, 4, 5 }, { 2, 4, 6, 4, 1 },
				{ 3, 4, 7, 9, 2 } };
		int[][] arr2 = new int[3][5];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 5; j++) {
				if (i == 0) {
					if (j == 0) {
						arr2[i][j] = arr1[i][j];
					} else {
						arr2[i][j] = arr2[i][j - 1] + arr1[i][j];
					}
				} else {
					if (j == 0) {
						arr2[i][j] = arr2[i - 1][j] + arr1[i][j];
					} else {
						arr2[i][j] = arr2[i - 1][j]
								+ (arr2[i][j - 1] - arr2[i - 1][j - 1])
								+ arr1[i][j];
					}
				}
				System.out.print(arr1[i][j] + "\t");
			}
			System.out.println();
		}
		System.out.println("----------------------------------------");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print(arr2[i][j] + "\t");
			}
			System.out.println();
		}
	}

	@Test
	public void firstJoinPoint() {
		//	单向链表 pk 双向链表
		
	}
	
	public int add(Node node) {
		int total = 0;
		if (node != null) {
			total += node.num;
			total += add(node.left);
			total += add(node.right);
			return total;
		} else {
			return total;
		}
	}

	private Node generateTree() {
		Node root = new Node(10);
		root.left = new Node(4);
		root.right = new Node(6);
		root.left.left = new Node(23);
		root.left.right = new Node(11);
		root.right.left = new Node(24);
		root.right.right = new Node(9);
		return root;
	}

	public class Node {
		int num;
		Node left;
		Node right;

		public Node(int num) {
			this.num = num;
		}
	}
}
