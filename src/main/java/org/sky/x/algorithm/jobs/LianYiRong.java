package org.sky.x.algorithm.jobs;

/**
 * 假设有从12到22的数字，但是中间有部分数据确实 查找连续的最长的数字段，和连续的最长的缺失数字段
 * 
 * 
 * @author sam
 *
 */
public class LianYiRong {

	static int rangeLength = 20;
	static int rangeStart = 10;

	public static void main(String[] args) {
		int[] data = new int[] { 12, 13, 14, 18, 19, 25};
		test(data);
	}

	private static void test(int[] data) {
		int[] arr = new int[rangeLength];
		for (int i = 0; i < data.length; i++) { // 数据填充到数组
			int idx = data[i] - rangeStart;
			arr[idx] = data[i];
		}

		int maxEmptyIdx = -1;
		int maxEmptyLength = 0;
		int tmpIdx = -1;
		int tmpLength = 0;
		boolean inEmpty = false;
		for (int i = 0; i < arr.length; i++) {
			int curItem = arr[i];
			if (curItem == 0) { // 进入空数据
				if (tmpLength == 0) {
					tmpIdx = i;
					inEmpty = true;
				}
				tmpLength++;
			} else {
				if (inEmpty) { // 退出空数据
					if (maxEmptyLength < tmpLength) {
						maxEmptyIdx = tmpIdx;
						maxEmptyLength = tmpLength;
					}
					tmpLength = 0;
					inEmpty = false;
				}
			}
		}
		if (maxEmptyLength < tmpLength) {
			maxEmptyIdx = tmpIdx;
			maxEmptyLength = tmpLength;
		}
		System.out.println((rangeStart + maxEmptyIdx) + "~" + maxEmptyLength);
	}
}
