package org.sky.x.algorithm.search;

import org.sky.x.algorithm.ArrayUtil;
import org.sky.x.algorithm.sort.MethodsOfSort;

public class Search {

	/**
	 * <pre/>
	 * 二分查找，返回索引，如果找不到返回空
	 * 
	 * @author sam.xie
	 * @date Apr 26, 2019 11:13:00 AM
	 * @param nums
	 * @param target
	 * @return
	 */
	public static int binarySearch(int[] nums, int target) {
		int low = 0;
		int high = nums.length - 1;
		int middle;
		while (low <= high) {
			middle = (low + high) / 2;
			if (nums[middle] < target) {
				low = middle + 1;
			} else if (nums[middle] > target) {
				high = middle - 1;
			} else {
				return middle;
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		int[] nums = {3};
		System.out.println(binarySearch(nums, 4));
	}
}
