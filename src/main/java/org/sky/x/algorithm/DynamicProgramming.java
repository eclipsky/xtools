package org.sky.x.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class DynamicProgramming {

	/**
	 * <pre/>
	 * 爬楼梯，每次可以踏1阶或2阶，爬完n阶有多少种情况
	 * 
	 * @author sam.xie
	 * @date Apr 23, 2019 11:51:43 AM
	 * @param n
	 * @return
	 */
	public int climbStairs(int n) {
		if (n == 1) {
			return 1;
		}
		int[] dp = new int[n + 1]; // dp[n]表示总阶梯为n的台阶有几种走法
		dp[1] = 1;
		dp[2] = 2;
		for (int i = 3; i <= n; i++) {
			dp[i] = dp[i - 1] + dp[i - 2];
			System.out.println("台阶数：" + i + " -> " + "组合：" + dp[i]);
		}
		return dp[n];
	}

	/**
	 * <pre/>
	 * 矩阵两个顶点的最短路径和
	 * 
	 * @author sam.xie
	 * @date Apr 23, 2019 12:13:17 PM
	 * @param grid
	 * @return
	 */
	public int minPathSum(int[][] grid) {
		int row = grid.length;
		int col = grid[0].length;
		int[][] dp = new int[row][col];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				dp[i][j] = grid[i][j];
				if (i != 0 || j != 0) {
					dp[i][j] += i == 0 ? dp[i][j - 1]
					        : j == 0 ? dp[i - 1][j]
					                : Math.min(dp[i - 1][j], dp[i][j - 1]);
				}
			}
		}
		return dp[row - 1][col - 1];
	}

	/**
	 * <pre/>
	 * 给定不同面额的硬币 coins 和一个总金额
	 * amount。编写一个函数来计算可以凑成总金额所需的最少的硬币个数。如果没有任何一种硬币组合能组成总金额，返回 -1。
	 * 
	 * @author sam.xie
	 * @date Apr 23, 2019 1:39:24 PM
	 * @param coins
	 * @param amount
	 * @return
	 */
	public int coinChange(int[] coins, int amount) {
		if (amount == 0)
			return 0;
		Arrays.sort(coins);
		int[] dp = new int[amount + 1];
		dp[0] = 0;
		for (int i = 1; i <= amount; i++) {
			dp[i] = 99999;
			for (int j = 0; j < coins.length; j++) {
				if (coins[j] <= i) {
					dp[i] = Math.min(dp[i - coins[j]] + 1, dp[i]);
				}
			}
		}
		return dp[amount] == 99999 ? -1 : dp[amount];
	}
	
	/**
	 * <pre/>
	 * fibonacci计算 递归计算
	 * 
	 * @author sam.xie
	 * @date Apr 26, 2019 10:40:02 AM
	 * @param n
	 * @return
	 */
	public int fibRecursion(int n) {
		if (n == 1)
			return 1;
		if (n == 2)
			return 2;
		return fibRecursion(n - 1) + fibRecursion(n - 2);
	}

	/**
	 * <pre/>
	 * finonacci计算 递归算法，但是保存了中间结果，减少重复计算（自顶向下的备忘录法）
	 * 
	 * @author sam.xie
	 * @date Apr 26, 2019 10:48:08 AM
	 * @param n
	 * @return
	 */
	public int fibRecursionWithMem(int n) {
		int[] mem = new int[n + 1];
		for (int i = 0; i <= n; i++) {
			mem[i] = -1;
		}
		return fibMem(n, mem);
	}

	public int fibMem(int n, int[] mem) {
		if (mem[n] != -1) {
			return mem[n];
		}
		if (n == 1) {
			mem[n] = 1;
			return 1;
		}
		if (n == 2) {
			mem[n] = 2;
			return 2;
		}
		return fibMem(n - 1, mem) + fibMem(n - 2, mem);
	}

	public int fibDP(int n) {
		// int[] mem = new int[n + 1];
		// mem[0] = 1;
		// mem[1] = 1;
		// for (int i = 2; i <= n; i++) {
		// mem[i] = mem[i - 1] + mem[i - 2];
		// }
		// return mem[n];
		int pre_1 = 1;
		int pre_2 = 1;
		int cur = 0;
		for (int i = 2; i <= n; i++) {
			cur = pre_1 + pre_2;
			pre_2 = pre_1;
			pre_1 = cur;
		}
		return cur;
	}

	/**
	 * <pre/>
	 * 不同单位长度的钢筋，价格不一样，现在有一段总长度为n的钢筋，如何切割能卖出最高的加钱 <br>
	 * 1 2 3 4 5 6 <br>
	 * 3 6 8 10 14 17
	 * 
	 * @author sam.xie
	 * @date Apr 26, 2019 4:20:21 PM
	 * @param p
	 * @param n
	 * @return
	 */
	public int cutSteelRecursion(int[] p, int n) {
		if (n == 0)
			return 0;
		int r = 0;
		for (int i = 1; i <= n; i++) {
			r = Math.max(r, p[i - 1] + cutSteelRecursion(p, n - i));
		}
		return r;
	}

	public void testFibnacci() {
		int n = 40;
		long start1 = System.currentTimeMillis();
		int num1 = fibRecursion(n);
		long start2 = System.currentTimeMillis();
		int num2 = fibRecursionWithMem(n);
		long start3 = System.currentTimeMillis();
		int num3 = fibDP(n);
		long start4 = System.currentTimeMillis();
		System.out.println(num1 + " -> " + (start2 - start1));
		System.out.println(num2 + " -> " + (start3 - start2));
		System.out.println(num3 + " -> " + (start4 - start3));
	}

	public static void main(String[] args) {
		DynamicProgramming test = new DynamicProgramming();
		String name = "sam";
		int[] array = { 1, 2, 8, 5 };
		List<List<Integer>> arrayList = new ArrayList<List<Integer>>();
		Stack stack = new Stack<>();
		stack.pop();
		test.testFibnacci();
	}

}
