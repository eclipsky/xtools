package org.sky.x;

import org.apache.commons.exec.*;
import org.junit.Test;
import sun.jvmstat.monitor.MonitoredHost;
import sun.jvmstat.monitor.MonitoredVm;
import sun.jvmstat.monitor.MonitoredVmUtil;
import sun.jvmstat.monitor.VmIdentifier;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ProcessUtil {

    @Test
    public void getRuntimeProcess(){
        Runtime rt = Runtime.getRuntime();

    }

    @Test
    public void getAllJavaProcess() throws Exception{

        // 获取当前java进程号
        RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
        System.out.println(runtimeMXBean.getName());


        // 获取监控主机
        MonitoredHost local = MonitoredHost.getMonitoredHost("localhost");
        // 取得所有在活动的虚拟机集合
        Set<?> vmlist = new HashSet<Object>(local.activeVms());
        // 遍历集合，输出PID和进程名
        for(Object process : vmlist) {
            MonitoredVm vm = local.getMonitoredVm(new VmIdentifier("//" + process));
            // 获取类名
            String processname = MonitoredVmUtil.mainClass(vm, true);
            System.out.println(process + " -> " + processname);
        }
    }

    @Test
    public void exeCmd(){
        String os = System.getenv("OS").toLowerCase();
        String path="D:\\itools\\ffmpeg\\bin\\";
        String cmd = path + "ffmpeg";
        if(os.contains("windows")){
            cmd = cmd + ".exe";
        }
        String[] args = {" -h"};
        try{
            Process process = Runtime.getRuntime().exec(cmd);
//            process = Runtime.getRuntime().exec(cmd, args);
            BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(process.getInputStream())));
            String line;
            while((line = reader.readLine()) != null){
                System.out.println(line);
            }
            process.waitFor();
            if (process.exitValue() != 0) {
                System.out.println("error! exitCode -> " + process.exitValue());
            }
            reader.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public  void exeCmd2() throws IOException, InterruptedException {
        CommandLine cmdLine = new CommandLine("AcroRd32.exe");
        cmdLine.addArgument("/p");
        cmdLine.addArgument("/h");
        cmdLine.addArgument("${file}");
        HashMap map = new HashMap();
        map.put("file", new File("invoice.pdf"));
        cmdLine.setSubstitutionMap(map);

        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

        ExecuteWatchdog watchdog = new ExecuteWatchdog(60*1000);
        Executor executor = new DefaultExecutor();
        executor.setExitValue(1);
        executor.setWatchdog(watchdog);
        executor.execute(cmdLine, resultHandler);

        // some time later the result handler callback was invoked so we
        // can safely request the exit value
//        int exitValue = resultHandler.waitFor();
        return;
    }

    public static void main(String[] args) {

    }
}
