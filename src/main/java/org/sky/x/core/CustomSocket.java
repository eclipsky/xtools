package org.sky.x.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 */

class SubSocket extends Socket {
	public void handle() throws IOException {
		BufferedReader reader = new BufferedReader(
		        new InputStreamReader(this.getInputStream()));
		String s = "";

		while (!(s = reader.readLine()).equals("quit")) {
			System.out.println("client: " + s);
			String resp = "";
			if (s.contains("who")) {
				resp = "server: i'm sam\r\n";
				response(resp);
			} else {
				resp = "server: i don't know what are you talking about\r\n";
				response(resp);
			}
		}
		this.close();
	}

	public void response(String resp) throws IOException {
		System.out.print(resp);
		OutputStream os = this.getOutputStream();
		os.write(resp.getBytes());
		os.flush();
	}

}

class SubServerSocket extends ServerSocket {

	/**
	 * @throws IOException
	 */
	public SubServerSocket(int port) throws IOException {
		super(port);
		// TODO Auto-generated constructor stub
	}

	public Socket accept() throws IOException {
		Socket socket = new SubSocket();
		/**
		 */
		implAccept(socket);
		return socket;
	}

}

public class CustomSocket {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		SubServerSocket subServer = new SubServerSocket(1234);
		subServer.setSoTimeout(0);
		while (true) {
			try {
				SubSocket subSocket = (SubSocket) subServer.accept();
				subSocket.setKeepAlive(true);
				subSocket.handle();
			} catch (Exception e) {
				subServer.close();
			} finally {
				subServer.close();
				break;
			}
		}
	}

}
