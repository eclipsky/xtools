package org.sky.x.core;

import java.util.*;

/**
 */
public class OverRideLoad {
	public static void main(String[] args){
		Sam sam = new Sam();
		System.out.println((new Sam().a));
	}
}

class Boy{
	int a = 5;
	int b =10;
	List getList(int a){
		return new LinkedList();
	}
}

class Sam extends Boy{
	int a = 6;
	int getA(){
		return super.a;
	}
	int getB(){
		return b;
	}
	ArrayList getAList(int a){
		return new ArrayList();
	}
}
