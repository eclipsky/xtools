package org.sky.x.core;

import java.util.ArrayList;
import java.util.List;

public class Initialization {

    public static void main(String[] args) {
        Class t = InitClass.class;
//        InitClass.test();
//        System.out.println(InitClass.list.size());
    }

}

class InitClass{

    public static List<Integer> list;

    static{
        init();
    }

    static void init(){
        list = new ArrayList<Integer>();
        test();
    }

    static void test(){
        System.out.println("list=" + list);
    }
}
