package org.sky.x.network;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * sample 
 * @author sam.xie
 * @date Dec 26, 2016 4:04:59 PM
 * @version 1.0
 */
public class InetAddressDemo{

	public static void main(String[] args) throws UnknownHostException{
		String host = "localhost";//127.0.0.1,
		byte[] bytes = {127,0,0,1};
		InetAddress[] addrs = InetAddress.getAllByName("www.google.com");//getByAddress(bytes)
		for(InetAddress addr : addrs){
			byte[] tmp = addr.getAddress();
			System.out.println(addr.getHostAddress()+"\t"+addr.getHostName());
		}
		
	}
}
