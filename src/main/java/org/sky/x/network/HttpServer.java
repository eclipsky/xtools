package org.sky.x.network;

import java.net.InetSocketAddress;

import java.net.ServerSocket;
import java.io.IOException;

/**
 * 
 * @author sam.xie
 * @date Dec 26, 2016 4:05:12 PM
 * @version 1.0
 */
public class HttpServer {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ServerSocket server = new ServerSocket();
		server.setReuseAddress(true);
		server.bind(new InetSocketAddress("localhost", 80));
		System.out.print("isClosed():" + server.isClosed());
		System.out.print("isBound():" + server.isBound());
		while (true) {
			server.close();
			// Socket socket = server.accept();
			// new ServerThread(socket);
			// server.close();
			// socket.close();
		}
	}

}