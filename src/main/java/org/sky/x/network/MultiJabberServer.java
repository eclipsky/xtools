package org.sky.x.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 */
public class MultiJabberServer {
	public static final int PORT = 8080;

	public static void main(String[] args) throws IOException {
		ServerSocket s = new ServerSocket(PORT);
		System.out.println("Server start");
		try {
			while (true) {
				Socket socket = s.accept();
				new ServerThread(socket);
			}
		} finally {
			s.close();
		}
	}
}
