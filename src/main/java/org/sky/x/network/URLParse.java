package org.sky.x.network;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class URLParse {
	public static void main(String[] args) throws UnsupportedEncodingException {
		String url = "https://www.baidu.com/s?wd=什么 值得买";
		String en =URLEncoder.encode(url, "utf8");
		System.out.println(en);
		System.out.println(URLDecoder.decode(en, "utf8"));
	}
}
