package org.sky.x.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 */
public class JabberServer {
    public static final int port = 8080;

    public static void main(String[] args) throws Exception {
        ioServer();
    }

    public static void nioServer() throws Exception{
        ServerSocket server = new ServerSocket(port);
    }

    public static void ioServer() throws Exception {
        ServerSocket server = new ServerSocket(port);
        try {
            while (true) {
                System.out.println("started:" + server);
                Socket socket = server.accept();
                System.out.println("Connection accepted:" + socket);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                while (true) {
                    String str = in.readLine();
                    System.out.println(server.isClosed());
                    if (str.equals("end")) break;
                    System.out.println("Echoing:" + str);
                    out.write("from the server:" + str + "\r\n");
                    out.flush();
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
//			server.close();
        }
//		System.out.println(server.isClosed());
    }
}
