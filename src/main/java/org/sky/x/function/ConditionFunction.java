package org.sky.x.function;

@FunctionalInterface
public interface ConditionFunction {
    boolean isTrue();
}
