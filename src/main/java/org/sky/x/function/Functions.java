package org.sky.x.function;

import java.util.List;

public class Functions {

    /**
     * 列表按指定size分成多批，并调用函数处理，可用于数据库批量插入等场景
     *
     * @param func
     * @param list
     * @param batchSize
     */
    public static void batchInsert(BatchFunction func, List list, int batchSize){
        if(null == list || list.size() == 0){
            return;
        }
        int fromIdx = 0;
        int toIdx = fromIdx + batchSize;
        while(toIdx < list.size()){
            func.batch(list.subList(fromIdx, toIdx));
            fromIdx = toIdx;
            toIdx += batchSize;
        }
        func.batch(list.subList(fromIdx, list.size()));
    }
    public static void batchInsert(BatchFunction func, List list) {
        batchInsert(func, list, 200);
    }

    /**
     * 把一组字符串值映射到另一组值（通过value获取name等）
     *
     * @param func
     * @param names
     * @return
     */
    public static String[] convert2Desc(DescFunction func, String ... names){
        if(null == names){
            return null;
        }
        String[] descArray = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            try {
                descArray[i] = func.getDesc(names[i]);
            }catch (Exception e){
                descArray[i] = "未知";
            }
        }
        return descArray;
    }

    public static String convert2Desc(DescFunction func, String name){
        try{
            if(null == name){
                return null;
            }else{
                return func.getDesc(name);
            }
        }catch (Exception e){
            return "未知";
        }
    }

    /**
     * 简单的重试函数，包含重试逻辑，重试次数，间隔时间，退出条件等
     * @param func
     * @param retryTimes
     * @param intervalMs
     * @param breakCondition
     * @return
     */
    public static Object retry(RetryFunction func, int retryTimes, long intervalMs, ConditionFunction breakCondition){
        Object result = null;
        while(retryTimes > 0){
            result = func.retry();
            if(breakCondition.isTrue()) break;
            if(intervalMs > 0){
                try {
                    Thread.sleep(intervalMs);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            retryTimes--;
        }
        return result;
    }
}
