package org.sky.x.function;

import java.util.List;

@FunctionalInterface
public interface BatchFunction {
    void batch(List data);
}
