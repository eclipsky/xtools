package org.sky.x.function;

@FunctionalInterface
public interface DescFunction {
    String getDesc(String name);
}
