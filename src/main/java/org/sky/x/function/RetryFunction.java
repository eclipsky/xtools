package org.sky.x.function;

@FunctionalInterface
public interface RetryFunction {
    Object retry();
}
