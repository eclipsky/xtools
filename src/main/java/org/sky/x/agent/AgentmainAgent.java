package org.sky.x.agent;

import java.lang.instrument.Instrumentation;

public class AgentmainAgent {
    private static Instrumentation INST;
    public static void agentmain(String agentArgs, Instrumentation inst) {
        INST = inst;
        process();
    }
    private static void process() {
        INST.addTransformer((loader, className, clazz, protectionDomain, byteCode) -> {
            System.out.println(String.format("Agentmain process by ClassFileTransformer,target class = %s", className));
            return byteCode;
        } ,true);
        try {
            INST.retransformClasses(Class.forName("org.sky.x.agent.AgentTargetSample"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
