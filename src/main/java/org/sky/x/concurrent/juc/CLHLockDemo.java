package org.sky.x.concurrent.juc;

import com.google.common.base.Stopwatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

public class CLHLockDemo {
    CountDownLatch start;
    CountDownLatch stop;
    CLHLock lock = new CLHLock();
//    ReentrantLock lock = new ReentrantLock();
    int i = 0;
    public void inc(){
        // 使用CLHLock性能很不穩定，偶尔会出现10s+的情况，CPU用满，why?
        // 重复测试部分场景使用CLHLock比ReentrantLock性能更高，why?
        lock.lock();
        i++;
//        System.out.println(Thread.currentThread() + " -> " + i);
        lock.unlock();
    }

    public void multiInc(int threadNum){
        for(int i =0;i < threadNum; i++){
            new Thread(() -> {
                try {
                    start.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for(int j=0;j <100;j++){
                    inc();
                }
                stop.countDown();
            }).start();
            start.countDown();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        CLHLockDemo demo = new CLHLockDemo();
        int threadNum = 10;
        demo.start = new CountDownLatch(threadNum);
        demo.stop = new CountDownLatch(threadNum);

        Stopwatch stop = Stopwatch.createStarted();
        demo.multiInc(threadNum);
        demo.stop.await();
        System.out.println(demo.i + " -> " + stop.stop());
    }
}

