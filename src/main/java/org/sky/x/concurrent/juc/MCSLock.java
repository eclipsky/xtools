package org.sky.x.concurrent.juc;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @see CLHLock
 * <href a = "https://www.cnblogs.com/sanzao/p/10567529.html"/>
 *
 *
 * CLHLock vs MCSLock
 * 1、CLHLock记录prev节点引用，MCSLock记录next节点引用
 * 2、CLHLock在前一个节点自旋，MCSLock在当前节点自旋
 * 3、
 */
public class MCSLock {

    private ThreadLocal<Node> node = ThreadLocal.withInitial(() -> new Node());
    private AtomicReference<Node> tail = new AtomicReference<>();

    public void lock(){
        Node curr = node.get(); // 1
        curr.locked = true; // 2
        Node prev = tail.getAndSet(curr); // 3
        if(prev != null){  // 4
            prev.next = curr;  // 5
            while(curr.locked);  // 6
        }
    }

    public void unlock(){
        Node curr = node.get();
        if(curr.next == null){
            // 当前节点就是最后一个节点
            if(tail.compareAndSet(curr, null)){
                return;
            }else{
                // 当前节点不是最后一个节点，但是next节点也不存在，如何会出现这种情况
                /**
                 * lock方法第3步执行后，tail节点已经指向了next节点，还没执行第5步，next未赋值，就出现了curr.next==null的情况
                 * 此处自旋等待
                 */
                while(curr.next == null);
            }

        }
        curr.next.locked = false;
        curr.next = null;
    }

    class Node{
         public volatile boolean locked = false;
         public volatile Node next;
    }
}
