package org.sky.x.concurrent.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyReadWriteLock {
    private ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private Lock readLock = rwl.readLock();
    private Lock writeLock = rwl.writeLock();
    private Condition readCondition = readLock.newCondition();
    private Condition writeCondition = writeLock.newCondition();

    ThreadLocal holdCounterTL = ThreadLocal.withInitial(() -> new HoldCounter());
    private Data data = new Data(0);
    private volatile boolean cacheValid;

    static final class HoldCounter {
        int count = 0;
        // Use id, not reference, to avoid garbage retention
        final long tid = 0L; // getThreadId(Thread.currentThread());
    }

    public static void main(String[] args) {
        MyReadWriteLock demo = new MyReadWriteLock();
        demo.processCachedData();
    }

    /**
     * 处理缓存数据
     */
    public void processCachedData() {
        // 获取
        rwl.readLock().lock();
        if (!cacheValid) {
            // Must release read rwl before acquiring write rwl
            rwl.readLock().unlock();
            rwl.writeLock().lock();
            try {
                // Recheck state because another thread might have
                // acquired write rwl and changed state before we did.
                if (!cacheValid) {
                    data = new Data(1);
                    cacheValid = true;
                }
                // Downgrade by acquiring read rwl before releasing write rwl
                rwl.readLock().lock();
            } finally {
                rwl.writeLock().unlock(); // Unlock write, still hold read
            }
        }

        try {
            use(data);
        } finally {
            rwl.readLock().unlock();
        }
    }

    private void use(Data data){
        System.out.println("use data -> " + data.id);
    }

    public class Data{
        public int id;
        public Data(int id){
            this.id = id;
        }
    }
}
