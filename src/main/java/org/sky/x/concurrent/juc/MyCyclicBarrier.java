package org.sky.x.concurrent.juc;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class MyCyclicBarrier {
	final CyclicBarrier barrier;
	final int MAX_TASK;
	static Random random = new Random();

	static CountDownLatch latch = new CountDownLatch(1);

	public MyCyclicBarrier(int cnt) {
		// barrier = new CyclicBarrier(cnt + 1);
		barrier = new CyclicBarrier(cnt);
		MAX_TASK = cnt;
	}

	public void doWork(final int id) {
		new Thread(() -> {
			try {
				Thread.sleep(random.nextInt(10) * 100);
				int index = barrier.await();
				doWithIndex(id, index);
			} catch (InterruptedException e) {
				return;
			} catch (BrokenBarrierException e) {
				return;
			}
		}).start();
	}

	private void doWithIndex(int id, int index) {
		if (index == MAX_TASK / 3) {
			System.out.println(id + " done, left 30%");
		} else if (index == MAX_TASK / 2) {
			System.out.println(id + " done, left 50%");
		} else if (index == 0) {
			System.out.println(id + " done, run over >>>");
			latch.countDown();
		} else {
			if (id >= 0) {
				System.out.println(id + " done");
			}
		}

	}

	public void waitForNext() {
		try {
			doWithIndex(-1, barrier.await());
		} catch (InterruptedException e) {
			return;
		} catch (BrokenBarrierException e) {
			return;
		}
	}

	public static void main(String[] args) throws Exception {
//		test();
		int SHARED_SHIFT   = 16;
		int SHARED_UNIT    = (1 << SHARED_SHIFT);
		int MAX_COUNT      = (1 << SHARED_SHIFT) - 1;
		int EXCLUSIVE_MASK = (1 << SHARED_SHIFT) - 1;

		int c = 10;
		System.out.println(c >>> SHARED_SHIFT); // sharedCount
		System.out.println(c & EXCLUSIVE_MASK); // exclusiveCount
	}

	/**
	 * 依次提交100个任务，每提交10个任务检查是否需要等待任务都完成
	 */
	public static void test(){
		final int count = 10;
		MyCyclicBarrier demo = new MyCyclicBarrier(count);
		for (int i = 0; i < 30; i++) {
			demo.doWork(i);
			// if ((i + 1) % count == 0) {
			// demo.waitForNext();
			// }
//			if ((i + 1) % count == 0) {
//				latch.await();
//				latch = new CountDownLatch(1);
//			}
		}
	}
}
