package org.sky.x.concurrent.juc;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class MySemaphore {

	private static Random random = new Random();
	private static Semaphore semaphore;
	private static CountDownLatch latch;

	static class Resource implements Runnable {

		private int id;

		public Resource(int id) {
			this.id = id;
		}

		@Override
		public void run() {
			try {
				semaphore.acquire();
				System.out.println("no [" + id + "] start");
				Thread.sleep(random.nextInt(5) * 100);
				System.out.println("no [" + id + "] over");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				semaphore.release();
				latch.countDown();
			}

		}
	}

	public static void main(String args[]) throws InterruptedException {
		ExecutorService execuror = Executors.newCachedThreadPool();
		int num = 10;
		System.out.println("ready, go!");
		semaphore = new Semaphore(2);
		latch = new CountDownLatch(num);
		for (int i = 0; i < num; i++) {
			execuror.submit(new Resource(i + 1));
		}
		execuror.shutdown();
		latch.await();
		System.out.println("every one done!");
	}

}
