package org.sky.x.concurrent.juc;

import java.util.concurrent.locks.ReentrantLock;

class MyReentrantLock extends Thread {

    ReentrantLock lock;

    public MyReentrantLock(String name, ReentrantLock lock) {
        this.setName(name + "");
        this.lock = lock;
    }

    public void run() {
        try {
            lock.lock();
            System.out.println(this.getName() + " get lock：" + lock);
            Thread.sleep((int) (Math.random() * 1000));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println(this.getName() + " release lock: " + lock);
            lock.unlock();
        }
    }

    public static void main(String args[]) {
        ReentrantLock lock = new ReentrantLock();
        for (int i = 0; i < 3; i++) {
            new MyReentrantLock("thread-" + i, lock).start();
        }
    }
}