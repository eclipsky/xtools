package org.sky.x.concurrent.juc;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ProducerConsumerLock {
    public static void main(String[] args) {
        Warehouse wh = new Warehouse(30);
        Thread producer1 = new Thread(new Producer("producer-1", wh));
        Thread producer2 = new Thread(new Producer("producer-2", wh));
        Thread consumer1 = new Thread(new Consumer("consumer-1", wh));
        Thread consumer2 = new Thread(new Consumer("consumer-2", wh));
        System.out.println("init stock:" + wh.getStock());
        consumer1.start();
        // consumer2.start();
        producer1.start();
        // producer2.start();
        while (Thread.activeCount() == 1) {
            /* 只有主线程活动 */
            System.out.println("结束生产消费模式");
        }
    }

    static class Warehouse {
        private int stock = 10;
        public static final int min = 10;
        public static final int max = 50;
        public Lock lock = new ReentrantLock();

        public Warehouse(int stock) {
            this.stock = stock;
        }

        public void produce(String name, int num) {
             lock.lock();
            int oldStock = stock;
            try {
                if (oldStock + num > Warehouse.max) {
                    System.out.println(name + ":" + num + ", warehouse is full, waiting...");
                } else {
                    stock += num;
                    System.out.println("before:" + oldStock + ", " + name + ":" + num + ", current stock:" + stock);
                }
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
             lock.unlock();
        }

        public void consume(String name, int num) {
            lock.lock();
            int oldStock = stock;
            try {
                if (oldStock - num < Warehouse.min) {
                    System.out.println(name + ":" + num + ", warehouse is not enough, waiting...");
                } else {
                    stock -= num;
                    System.out.println("before:" + oldStock + ", " + name + ":" + num + ", current stock:" + stock);
                }
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            lock.unlock();
        }

        public int getStock() {
            return stock;
        }

        public void setStock(int stock) {
            this.stock = stock;
        }

    }

    static class Producer implements Runnable {

        private Warehouse wh;
        private String name;

        public Producer(String name, Warehouse wh) {
            this.name = name;
            this.wh = wh;
        }

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                int num = 3;// random.nextInt(100) % 17 + 1;
                wh.produce(name, num);
            }
        }
    }

    static class Consumer implements Runnable {

        private Warehouse wh;
        private String name;

        public Consumer(String name, Warehouse wh) {
            this.name = name;
            this.wh = wh;
        }

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                int num = 5; // random.nextInt(100) % 9 + 1;
                wh.consume(name, num);
            }
        }
    }
}