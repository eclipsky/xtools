package org.sky.x.concurrent.juc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <pre/>
 * 银行存取款操作
 * 
 * 1.使用条件变量和锁
 * 2.使用synchronized方法
 * 3.使用synchronized代码快
 * 
 * @author sam.xie
 * @date Sep 1, 2017 5:18:57 PM
 * @version 1.0
 */
public class SaveDrawTest {
    public static void main(String[] args) {
        // Account account = new AccountByCondition(1000);
        // Account account = new AccountBySyncMethod(1000);
        Account account = new AccountBySyncBlock(1000);
        ExecutorService executor = Executors.newFixedThreadPool(10);
        Thread save1 = new SaveThread("save-1", account, 200);
        Thread save2 = new SaveThread("save-2", account, 400);
        Thread save3 = new SaveThread("save-3", account, 900);
        Thread save4 = new SaveThread("save-4", account, 700);

        Thread draw1 = new DrawThread("draw-1", account, 500);
        Thread draw2 = new DrawThread("draw-2", account, 800);
        Thread draw3 = new DrawThread("draw-3", account, 700);
        Thread draw4 = new DrawThread("draw-4", account, 600);
        Thread draw5 = new DrawThread("draw-5", account, 600);

        executor.execute(save1);
        executor.execute(save2);
        executor.execute(save3);
        executor.execute(draw1);
        executor.execute(draw2);
        executor.execute(draw3);
        executor.execute(draw4);
        executor.execute(draw5);
        executor.execute(save4);

        executor.shutdown();
    }

    //
    static class SaveThread extends Thread {

        private String user;
        private Account account;
        private int amount;

        public SaveThread(String user, Account account, int amount) {
            this.user = user;
            this.account = account;
            this.amount = amount;
        }

        @Override
        public void run() {
            account.save(user, amount);
        }

    }

    static class DrawThread extends Thread {

        private String user;
        private Account account;
        private int amount;

        public DrawThread(String user, Account account, int amount) {
            this.user = user;
            this.account = account;
            this.amount = amount;
        }

        @Override
        public void run() {
            account.draw(user, amount);
        }

    }

    static abstract class Account {
        public int balance;

        public Account(int balance) {
            this.balance = balance;
            System.out.println("account balance:" + balance);
        }

        void save(String user, int amount) {

        }

        void draw(String user, int amount) {

        }
    }

    static class AccountByCondition extends Account {
        private ReentrantLock lock = new ReentrantLock();
        private Condition saveCond = lock.newCondition();
        private Condition drawCond = lock.newCondition();

        public AccountByCondition(int balance) {
            super(balance);
        }

        @Override
        public void save(String user, int amount) {
            lock.lock();
            balance += amount;
            System.out.println(user + ":" + amount + ", balance:" + balance);
            drawCond.signalAll();
            lock.unlock();
        }

        @Override
        public void draw(String user, int amount) {
            lock.lock();
            // if (balance - amount < 0) {
            // System.out.println(user + " draw:" + amount + ", balance:" + balance + " is not enough, waiting");
            // try {
            // drawCond.await();
            // } catch (Exception e) {
            // e.printStackTrace();
            // }
            // }
            check(user, amount);
            balance -= amount;
            System.out.println(user + ":" + amount + ", balance:" + balance);
            saveCond.signalAll();
            lock.unlock();
        }

        private void check(String user, int amount) {
            if (balance - amount < 0) {
                System.out.println(user + ":" + amount + ", balance:" + balance + " is not enough, waiting...");
                try {
                    drawCond.await();
                    check(user, amount);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    static class AccountBySyncMethod extends Account {

        public AccountBySyncMethod(int balance) {
            super(balance);
        }

        @Override
        public synchronized void save(String user, int amount) {
            balance += amount;
            System.out.println(user + " save:" + amount + ", balance:" + balance);
            this.notifyAll();
        }

        @Override
        public synchronized void draw(String user, int amount) {
            check(user, amount);
            balance -= amount;
            System.out.println(user + ":" + amount + ", balance:" + balance);
            this.notifyAll();
        }

        private void check(String user, int amount) {
            if (balance - amount < 0) {
                System.out.println(user + ":" + amount + ", balance:" + balance + " is not enough, waiting...");
                try {
                    this.wait();
                    check(user, amount);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    static class AccountBySyncBlock extends Account {

        public AccountBySyncBlock(int balance) {
            super(balance);
        }

        @Override
        public void save(String user, int amount) {
            synchronized (this) {
                balance += amount;
                System.out.println(user + " save:" + amount + ", balance:" + balance);
                this.notifyAll();
            }
        }

        @Override
        public void draw(String user, int amount) {
            synchronized (this) {
                check(user, amount);
                balance -= amount;
                System.out.println(user + ":" + amount + ", balance:" + balance);
                this.notifyAll();
            }
        }

        private void check(String user, int amount) {
            if (balance - amount < 0) {
                System.out.println(user + ":" + amount + ", balance:" + balance + " is not enough, waiting...");
                try {
                    this.wait();
                    check(user, amount);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
