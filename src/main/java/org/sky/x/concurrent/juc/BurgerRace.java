package org.sky.x.concurrent.juc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * <pre/>
 * @author sam.xie
 * who can eat 10 burgers first
 * if one man choked, how would it go
 */
public class BurgerRace {

	private CyclicBarrier barrier;
	private int nHorses;
	private ExecutorService executor;
	private List<Horse> horses = new ArrayList<Horse>();

	public static class Horse implements Runnable {
		private int horseId;
		private CyclicBarrier barrier;
		private int stride;
		private static final Random random = new Random(47);

		public Horse(int id, CyclicBarrier barrier) {
			this.horseId = id;
			this.barrier = barrier;
		}

		@Override
		public void run() {
			try {
				while (true) {
					if (!Thread.interrupted()) {
						stride += random.nextInt(5);
						barrier.await();
					} else {
						System.out.println(this + " interruptted");
						return;
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
			System.out.println("horse:" + horseId + " over, "
					+ Thread.currentThread());
		}

		public String toString() {
			return "horse:" + horseId;
		}

		public String printStride() {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < stride; i++) {
				sb.append("*");
			}
			return sb.append(horseId).toString();
		}

		public int getStride() {
			return stride;
		}
	}

	public BurgerRace(final int distance, final int nHorses, final int pauses) {
		this.nHorses = nHorses;
		this.executor = Executors.newCachedThreadPool();
		this.barrier = new CyclicBarrier(nHorses, new Runnable() {
			@Override
			public void run() {
				System.out.println(Thread.currentThread() + ", barrier open");
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < distance; i++) {
					sb.append("=");
				}
				System.out.println(sb.toString());
				for (Horse horse : horses) {
					System.out.println(horse.printStride());

				}
				for (Horse horse : horses) {
					if (horse.getStride() >= distance) {
						System.out.println(horse + " win!");
						executor.shutdownNow();
						return;
						/**
						 * barrier内部的run逻辑实际上是horse线程来运行的
						 * 这里如果不return，后面执行sleep会报错，同时由于interrupted状态位被清理了，
						 * barrier在Horse的run方法里面循环并不会中断
						 * ，此时可以使用Thread.currentThread().interrupt()重新设置中断未
						 */
					}
				}
				try {
					TimeUnit.MILLISECONDS.sleep(pauses);
				} catch (InterruptedException e) {
					// Thread.currentThread().interrupt();
					e.printStackTrace();
				}
				System.out.println(Thread.currentThread() + ", barrier close");
			}
		});
	}

	public void run() {
		for (int i = 1; i <= nHorses; i++) {
			Horse horse = new Horse(i, barrier);
			horses.add(horse);
			executor.execute(horse);
		}
	}

	public static void main(String[] args) {
		new BurgerRace(50, 5, 500).run();
	}
}
