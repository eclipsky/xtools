package org.sky.x.concurrent.juc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * <pre/>
 * @author sam.xie
 * Semaphore and CountDownLatch serves different purpose.
 * Use Semaphore to control thread access to resource.
 * Use CountDownLatch to wait for completion of all threads
 *
 */
public class MyCountDownLatch {

	public static void main(String[] args) throws InterruptedException {

		/**
		 * 等所有线程都启动了才开始
		 * 等所有线程都结束了才结束
		 */
		final CountDownLatch begin = new CountDownLatch(1);
		final CountDownLatch end = new CountDownLatch(10);
		final ExecutorService exec = Executors.newFixedThreadPool(1);

		for (int index = 0; index < 10; index++) {
			final int NO = index;
			Runnable run = new Runnable() {
				public void run() {
					try {
						begin.await();// 一直阻塞
						long start = System.currentTimeMillis();
						int sleep = (int) ((Math.random() + 1) * 1000);
						Thread.sleep(sleep);
						long end = System.currentTimeMillis();
						System.out.println("No." + NO + " arrived, start:"
								+ start + ", end:" + end + ", cost:"
								+ (end - start) + ", sleep:" + sleep);
					} catch (InterruptedException e) {
						;
					} finally {
						end.countDown();
					}
				}
			};
			new Thread(run).start();
			// exec.submit(run);
		}
		System.out.println("Game Start");
		begin.countDown();
		end.await();
		System.out.println("Game Over");
		exec.shutdown();
	}
}
