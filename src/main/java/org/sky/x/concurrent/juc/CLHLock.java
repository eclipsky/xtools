package org.sky.x.concurrent.juc;

import java.util.concurrent.atomic.AtomicReference;

/**
 * CLH锁
 * CLH也是一种基于单向链表(隐式创建)的高性能、公平的自旋锁，申请加锁的线程只需要在其前驱节点的本地变量上自旋
 * 从而极大地减少了不必要的处理器缓存同步的次数，降低了总线和内存的开销。
 * <href a = "https://blog.csdn.net/weixin_43893935/article/details/108256347" />
 */
//public class CLHLock {
//    // 尾巴，是所有线程共有的一个。所有线程进来后，把自己设置为tail
//    private final AtomicReference<Node> tail;
//    // 前驱节点，每个线程独有一个。
//    private final ThreadLocal<Node> pre;
//    // 当前节点，表示自己，每个线程独有一个。
//    private final ThreadLocal<Node> cur;
//
//    public CLHLock() {
//        this.tail = new AtomicReference<Node>(new Node());
//        this.cur = ThreadLocal.withInitial(() -> new Node());
//        this.pre = new ThreadLocal<>();
//    }
//
//    public void lock() {
//        // 获取当前线程的代表节点
//        Node node = cur.get();
//        // 将自己的状态设置为true表示获取锁。
//        node.locked = true;
//        // 将自己放在队列的尾巴，并且返回以前的值。第一次进将获取构造函数中的那个new Node
//        Node pred = tail.getAndSet(node);
//        // 把旧的节点放入前驱节点。
//        pre.set(pred);
//        // 判断前驱节点的状态，然后走掉。
//        while (pred.locked) {
//        }
//    }
//
//    public void unlock() {
//        // unlock. 获取自己的node。把自己的locked设置为false。
//        Node node = cur.get();
//        node.locked = false;
//        cur.set(pre.get());
//    }
//
//    class Node {
//        volatile boolean locked;
//    }
//
//}
//public class CLHLock {
//    //指向最后加入的线程
//    AtomicReference<Node> tail = new AtomicReference<>();
//    //当前线程持有的节点,使用ThreadLocal实现了变量的线程隔离
//    ThreadLocal<Node> node;
//    //前驱节点，使用ThreadLocal实现了变量的线程隔离
//    ThreadLocal<Node> preNode = new ThreadLocal<>();
//
//    public CLHLock() {
//        //初始化node
//        //线程默认变量的值,如果不Override这个函数，默认值为null
//        node = ThreadLocal.withInitial(() -> new Node());
//        //初始化tail，指向一个node，类似一个head节点，并且该节点locked属性为false
//        tail.set(new Node());
//    }
//
//    public void lock() {
//        //因为上面提到的构造函数中initialValue()方法，所以每个线程会有一个默认的值
//        //并且node的locked属性为false.
//        Node cur = node.get();
//        //修改为true，表示需要获取锁
//        cur.locked = true;
//        //获取这之前最后加入的线程，并把当前加入的线程设置为tail，
//        // AtomicReference的getAndSet操作是原子性的
//        Node preNode = tail.getAndSet(cur);
//        //设置当前节点的前驱节点
//        this.preNode.set(preNode);
//        //轮询前驱节点的locked属性，尝试获取锁
//        while (preNode.locked) {
//
//        }
//    }
//
//    public void unlock() {
//        //解锁很简单，将节点locked属性设置为false，
//        //这样轮询该节点的另一个线程可以获取到释放的锁
//        node.get().locked = false;
//        //当前节点设置为前驱节点，也就是上面初始化提到的head节点
//        node.set(preNode.get());
//    }
//
//    private class Node{
//        //默认不需要锁
//        private volatile boolean locked = false;
//    }
//}

/**
 * 有几个问题，
 * 1、如果不加锁直接解锁会如何
 *    不先执行加锁，那么这个node将不会与链表产生关联，执行解锁操作，也只是初始化一个node节点而已，不会对其他锁产生影响
 * 2、如果加锁等待，直接解锁会如何
 *    理论上不可能出现这种情况，ThreadLocal变量与线程关联，同一个线程内部不可能出现一段代码堵塞（节点自旋），还能执行其他代码
 * 3、是否可重入（在没有解锁前多次加锁）
 *    不可以，如果没有解锁，当前线程将一直堵塞在前一次的获取锁的过程中
 *
 */
public class CLHLock{

    private AtomicReference<Node> tail;
    private ThreadLocal<Node> prev;
    private ThreadLocal<Node> curr;

    public CLHLock(){
        tail = new AtomicReference<>(new Node());
        curr = ThreadLocal.withInitial(() -> new Node());
        prev = new ThreadLocal<>();
    }

    public void lock(){
        Node node = curr.get();
        node.locked = true;

        Node prevNode = tail.getAndSet(node);
        prev.set(prevNode);

        while (prevNode.locked){

        }
    }

    public void unlock(){
        Node node = curr.get();
        node.locked = false;
        // curr.set(new Node());
        curr.set(prev.get()); //
    }

    class Node{
        private volatile boolean locked = false;
    }
}

