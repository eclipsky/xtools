package org.sky.x.concurrent.juc;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class ThreadPoolTest {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        // ExecutorService executor = Executors.newFixedThreadPool(3);
        // ExecutorService executor = Executors.newCachedThreadPool();
        ExecutorService executor = Executors.newFixedThreadPool(3);
        executor.submit(new MyTask("task1"));
        executor.submit(new MyTask("task2"));
        executor.submit(new MyTask("task3"));
        Future<String> result4 = executor.submit(new MyFutureTask("task4"));
        Future<String> result5 = executor.submit(new MyFutureTask("task5"));
        while (true) {
            if (result5.isDone()) {
                System.out.println(result5.get());
                break;
            } else {
                Thread.sleep(200);
                System.out.println("task5 not done yet");
            }
        }
        System.out.println(result4.get());
        // executor.shutdown();
    }

    static class MyTask implements Runnable {
        String name;

        public MyTask(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " -> " + name);
        }
    }

    static class MyFutureTask implements Callable<String> {
        public static Random random = new Random();

        String name;

        public MyFutureTask(String name) {
            this.name = name;
        }

        @Override
        public String call() throws Exception {
            Thread.sleep(1000);
            return name + " -> " + random.nextInt(100);
        }

    }
}
