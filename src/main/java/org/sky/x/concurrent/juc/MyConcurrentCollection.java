package org.sky.x.concurrent.juc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

public class MyConcurrentCollection {
//
//	// @Test
//	public void concurrentHashMap() {
//		ConcurrentHashMap<Integer, String> infoMap = new ConcurrentHashMap<Integer, String>();
//		int key = 1;
//		String value = "sam";
//		String value2 = "jack";
//		System.out.println(infoMap.putIfAbsent(key, value));
//		System.out.println(infoMap.putIfAbsent(key, value));
//
//		System.out.println(infoMap.remove(key, value2));
//		System.out.println(infoMap.remove(key, value));
//	}
//
//	// @Test
//	public void mapHashCode() {
//		int size = 16; // 10000
//		String[] names = { "sam", "sara", "h&m", "tom", "clark", "tony" };
//		for (String name : names) {
//			int hashCode = name.hashCode();
//			System.out.println(name + ":" + hashCode + ":"
//					+ (hashCode & (size - 1)));
//		}
//	}

	@Test
	public void hashMap() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("sara", 18);
		map.put("sam", 19);
		map.put("tom", 20);
		Iterator<Entry<String, Integer>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
	//
	// public static void main(String[] args) {
	// MyConcurrentCollection test = new MyConcurrentCollection();
	// // test.mapHashCode();
	// test.hashMap();
	// }
}
