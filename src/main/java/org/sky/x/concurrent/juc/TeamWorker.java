package org.sky.x.concurrent.juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TeamWorker {

	private ExecutorService executor;
	private int tasks;
	private int workers;
	private CyclicBarrier barrier;

	public TeamWorker(int tasks, int workers) {
		this.executor = Executors.newCachedThreadPool();
		this.tasks = tasks;
		this.workers = workers;
		this.barrier = new CyclicBarrier(workers + 1, new Runnable() {
			@Override
			public void run() {
				try {
					TimeUnit.MILLISECONDS.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		});
	}

	public class Worker implements Runnable {

		public void run() {
			try {
				int index = barrier.await();
				noticeProcess(index); // 由于await会阻塞,这里通知并不会立马送达,并不能真实的反馈进度
			} catch (InterruptedException | BrokenBarrierException e) {
				// e.printStackTrace();
				return;
			}
		}
	}

	private void noticeProcess(int index) {
		if (index == workers / 3) {
			System.out.println("left 30%");
		} else if (index == workers / 2) {
			System.out.println("left 50%");
		} else if (index == 0) {
			System.out.println("run over");
		}
	}

	public void waitForNextBatchWork() {
		try {
			noticeProcess(barrier.await());
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
	}

	public void start() {
		for (int i = 0; i <= tasks; i++) {
			Worker worker = new Worker();
			executor.execute(worker);
			if ((i + 1) % workers == 0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(">>> work done -> " + i);
				waitForNextBatchWork();
			}
		}
		executor.shutdownNow();
	}

	public static void main(String[] args) {
		TeamWorker teamWorker = new TeamWorker(100, 10);
		teamWorker.start();
	}
}
