package org.sky.x.concurrent.juc;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ConsumerProducerTest {
    private int num = 100;
    public static void main(String[] args) throws InterruptedException {
        ConsumerProducerTest test = new ConsumerProducerTest();
        Warehouse warehouse = new Warehouse(100);
        Thread t1 = new Thread(new Consumer(warehouse, "c1"));
        Thread t2 = new Thread(new Producer(warehouse, "p1"));
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("game over");
    }

    static class Warehouse{
        int stock;
        int capacity;
        ReentrantLock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        private Warehouse(int capacity){
            this.capacity = capacity;
        }
    }

    static class Consumer implements Runnable{
        private Warehouse warehouse;
        private String id;
        public Consumer(Warehouse warehouse, String id){
            this.warehouse = warehouse;
            this.id = id;
        }
        @Override
        public void run() {
            while(true){
                synchronized (warehouse.lock){
//                    warehouse.lock.lock();
                    int toTake = RandomUtils.nextInt(5) + 1;
                    if(warehouse.stock - toTake < 0){
                        System.out.println(String.format("%s -> stock(%s) is not enough to consume(%s) ,waiting for producing", id, warehouse.stock, toTake));
                        try {
//                            warehouse.condition.await();
                            warehouse.lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }else{
                        System.out.println(String.format("%s -> stock(%s), take(%s), final stock(%s)", id, warehouse.stock, toTake, warehouse.stock-=toTake));
                        try {
                            Thread.sleep(2000);
//                            warehouse.condition.signalAll();
                            warehouse.lock.notifyAll();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
//                    warehouse.lock.unlock();
                }
            }
        }
    }
    static class Producer implements Runnable{
        private Warehouse warehouse;
        private String id;
        public Producer(Warehouse warehouse, String id){
            this.warehouse = warehouse;
            this.id = id;
        }
        @Override
        public void run() {
            while(true){
                synchronized (warehouse.lock) {
//                    warehouse.lock.lock();
                    int toPut = RandomUtils.nextInt(10) + 2;
                    if (warehouse.stock + toPut > warehouse.capacity) {
                        System.out.println(String.format("%s -> stock(%s) is reaching capacity(%s) if put(%s) ,waiting for consuming", id, warehouse.stock, warehouse.capacity, toPut));
                        try {
//                            warehouse.condition.await();
                            warehouse.lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println(String.format("%s -> stock(%s), put(%s), final stock(%s)", id, warehouse.stock, toPut, warehouse.stock += toPut));
                        try {
                            Thread.sleep(2000);
//                            warehouse.condition.signalAll();
                            warehouse.lock.notifyAll();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
//                    warehouse.lock.unlock();
                }
            }
        }
    }

    @Test
    public void test() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        int sleepMs = 1000 * 1000;
        Thread t = new Thread(() -> {
            try {
                Thread.sleep(sleepMs);
                latch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t.start();
        latch.await();
        System.out.println("game over");
    }
}
