package org.sky.x.concurrent.future;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class FutureDemo {
    public static void main(String[] args) throws Exception {
//        demo1();
        demo2();
    }

    public static void demo2() throws Exception{
        FutureTask task = new FutureTask(new Callable() {
            @Override
            public Object call() throws Exception {
                Thread.sleep(1000 * 10);
                return "ok";
            }
        });
        Thread t = new Thread(task);
        t.start();
        System.out.println(task.get());
    }

    public static void demo1(){
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread());
            }
        });
        t1.start();
        System.out.println(Thread.currentThread());
    }
}
