package org.sky.x.concurrent.thread;

public class ThreadLocalDemo {
    public ThreadLocal<Integer> local = new ThreadLocal<>();

    public static void main(String[] args){

    }

    class Calc<T> implements Runnable{

        ThreadLocal<T> local;

        public Calc(ThreadLocal<T> local){
            this.local = local;
        }

        @Override
        public void run() {
            Integer data = (Integer)local.get();
            data = 10;
        }
    }
}
