package org.sky.x.concurrent.thread;

public class ThreadDemo {

	static ThreadLocal<Integer> myids = new ThreadLocal<Integer>();

	static class MyJob implements Runnable {
		int sleepTime;

		MyJob(int sleepTime) {
			this.sleepTime = sleepTime;
		}

		@Override
		public void run() {
			try {
				Thread.sleep(sleepTime * 1000);
				myids.set(sleepTime);
				System.out.println("sleep over: " + myids.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Thread t1 = new Thread(new MyJob(3));
		Thread t2 = new Thread(new MyJob(5));

		t1.start();
		t2.start();
		t1.join();
		t2.join();
		
		System.out.println("main over");
	}
}
