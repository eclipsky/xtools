package org.sky.x.concurrent.thread;

public class IfSerialRun {
	private boolean isDone = false; // 如果这里的isDone不设置为volatile，t1线程可能永远都不会停
	Thread t1 = new Thread(new Runnable() {
		@Override
		public void run() {
			while (true) {
				if(isDone) {					
					System.out.println("it's over");
					break;
				}
			}
		}
	});

	Thread t2 = new Thread(new Runnable() {
		@Override
		public void run() {
			for (int i = 0; i < 10; i++) {
				try {
					Thread.sleep(100);
					System.out.println("i=" + i);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			isDone = true;
		}
	});

	public static void main(String[] args) {
		IfSerialRun test = new IfSerialRun();
		test.t1.start();
		test.t2.start();
	}
}
