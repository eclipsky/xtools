package org.sky.x.concurrent.thread;

import java.util.Random;

public class ProducerConsumerNew {
    public static void main(String[] args) {
        Warehouse wh = new Warehouse(30);
        Thread producer1 = new Thread(new Producer("producer-1", wh));
        Thread producer2 = new Thread(new Producer("producer-2", wh));
        Thread consumer1 = new Thread(new Consumer("consumer-1", wh));
        Thread consumer2 = new Thread(new Consumer("consumer-2", wh));
        System.out.println("init stock:" + wh.getStock());
        producer1.start();
        // producer2.start();
        consumer1.start();
        // consumer2.start();
        while (Thread.activeCount() == 1) {
            /* 只有主线程活动 */
            System.out.println("结束生产消费模式");
        }
    }
}

class Warehouse {
    private int stock = 10;
    public static final int min = 10;
    public static final int max = 50;

    public Warehouse(int stock) {
        this.stock = stock;
    }

    public void produce(int num) {
        stock += num;
    }

    public void consume(int num) {
        stock -= num;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

}

class Producer implements Runnable {

    private Warehouse wh;
    private String name;
    private static final Random random = new Random();

    public Producer(String name, Warehouse wh) {
        this.name = name;
        this.wh = wh;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (wh) {
                int num = random.nextInt(100) % 13 + 1;
                int oldStock = wh.getStock();
                try {
                    if (oldStock + num > Warehouse.max) {
                        System.out.println(name + ":" + num + ", warehouse is full, waiting...");
                        wh.wait();
                    } else {
                        wh.produce(num);
                        Thread.sleep(1000);
                        System.out.println(name + ":" + num + ", current stock:" + wh.getStock());
                        wh.notifyAll();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

class Consumer implements Runnable {

    private Warehouse wh;
    private String name;
    private static final Random random = new Random();

    public Consumer(String name, Warehouse wh) {
        this.name = name;
        this.wh = wh;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (wh) {
                int num = random.nextInt(100) % 7 + 1;
                int oldStock = wh.getStock();
                try {
                    if (oldStock - num < Warehouse.min) {
                        System.out.println(name + ":" + num + ", not enough, waiting...");
                        wh.wait();
                    } else {
                        wh.consume(num);
                        Thread.sleep(1000);
                        System.out.println(name + ":" + num + ", current stock:" + wh.getStock());
                        wh.notifyAll();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}