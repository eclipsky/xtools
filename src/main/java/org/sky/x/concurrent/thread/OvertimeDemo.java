package org.sky.x.concurrent.thread;

/**
 * 超时中断实现
 * 
 * @author sam.xie
 * @date Feb 20, 2017 12:51:45 PM
 * @version 1.0
 */
public class OvertimeDemo {
	public static void main(String[] args) {
		Task task = new Task(5);
		task.execute();
	}
}

class Task {
	private int status; // 1表示运行，2表示中断
	private long startTime;
	private int seconds;

	Task(int seconds) {
		this.seconds = seconds;
	}

	public void execute() {
		startTime = System.currentTimeMillis();
		TaskThread t = new TaskThread(this);
		Thread anotherThread = new Thread(t);
		anotherThread.start();
		System.out.println("任务开始...");
		while (true) {
			if (System.currentTimeMillis() - startTime >= (seconds * 1000)) {
				this.setStatus(2); // 设置中断
				anotherThread.interrupt();
				break;
			}
		}
		System.out.println("任务结束...");

	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}

class TaskThread implements Runnable {

	public static final int RUNNING = 1;
	public static final int INTERRUPT = 2;
	private Task task;

	public TaskThread(Task task) {
		this.task = task;
	}

	@Override
	public void run() {
		int i = 0;
		while (true) {
			if (task.getStatus() == INTERRUPT) {
				// 这里
				break;
			}
			System.out.println("线程运行秒数：" + i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
		}
	}

}
