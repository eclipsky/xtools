package org.sky.x.concurrent.thread;

public class WaitTest {

    public final Object obj = new Object();

    public static void main(String[] args) throws InterruptedException {
        WaitTest test = new WaitTest();
        Function func = new Function();
        Thread t1 = test.getThread(1, func, 50);
        t1.start();
        synchronized (t1) {
//            t1.wait();
            System.out.println("t1 done");
        }

    }

    public Thread getThread(final int id, final Function func, final int step) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized(this){                    
                    for (int i = 0; i < 5; i++) {
                        func.inc(step);
                        System.out.println(id + ":" + func.get());
                    }
                    notify();
                }
            }
        });
    }
}

class Function {
    private int i = 0;

    public void inc(int num) {
        synchronized (this) {
            i = i + num;
        }
    }

    public int get() {
        return i;
    }
}