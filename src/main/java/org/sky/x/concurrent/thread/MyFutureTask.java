package org.sky.x.concurrent.thread;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * 超时中断实现
 * 线程池中
 * @author sam.xie
 * @date Feb 20, 2017 12:51:45 PM
 * @version 1.0
 */
public class MyFutureTask {

	static ExecutorService executorService = Executors
			.newSingleThreadExecutor();

	public static void main(String[] args) {
		Random random = new Random();
		MyFutureTask task = new MyFutureTask();
		 task.execute0();
		for (int i = 0; i < 10; i++) {
//			task.execute1(i, 2 + random.nextInt(3));
		}
	}

	public void execute0() {
		Callable<String> callable = new Callable<String>() {
			@Override
			public String call() throws Exception {
				Thread.sleep(3 * 1000);
				return "执行完成";
			}
		};
		Future<String> future = executorService.submit(callable);
		System.out.println("执行开始");
		String result = null;
		try {
			result = future.get(2, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			result = "执行中断";
		} catch (ExecutionException e) {
			result = "执行错误";
		} catch (TimeoutException e) {
			result = "执行超时";
		} finally {
			future.cancel(true);
		}
		System.out.println(result);
		executorService.shutdown();
	}

	public void execute1(final int taskId, int timeout) {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		FutureTask<String> future = new FutureTask<String>(
				new Callable<String>() {
					@Override
					public String call() throws Exception {
						Thread.sleep(3 * 1000);
						return taskId + "-执行完成";
					}
				});
		executorService.submit(future); // 这列的noUseResult作何用？
		System.out.println(taskId + "-执行开始");
		String result = null;
		try {
			result = future.get(timeout, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			result = taskId + "-执行中断";
		} catch (ExecutionException e) {
			result = taskId + "-执行错误";
		} catch (TimeoutException e) {
			result = taskId + "-执行超时";
		} finally {
			future.cancel(true);
		}
		System.out.println(result);
		// executorService.shutdown();F
	}
}
