package org.sky.x.concurrent.thread;

public class Calculator extends Thread {
    private int total;

    public int getTotal() {
        return total;
    }

    public void run() {
        synchronized (this) {
            for (int i = 1; i <= 100; i++) {
                total += i;
            }
            notifyAll();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Calculator calc = new Calculator();
        calc.start();
        synchronized (calc) {
            calc.wait();
            System.out.println("total=" + calc.getTotal());
        }
    }
}