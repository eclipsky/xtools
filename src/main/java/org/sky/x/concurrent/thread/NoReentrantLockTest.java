package org.sky.x.concurrent.thread;

import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.omg.CORBA.PUBLIC_MEMBER;

public class NoReentrantLockTest {
	public Lock lock = new ReentrantLock();
	// public NoReentrantLock lock = new NoReentrantLock();
	public int result = 0;

	public static void main(String[] args) throws InterruptedException {
		NoReentrantLockTest queue = new NoReentrantLockTest();
		queue.calc(2, 5);
		Thread t1 = new Thread(new Runnable() {
			public void run(){
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		t1.start();
		t1.join();
	}

	public void calc(int a, int b) {
		lock.lock();
		System.out.println(a + b);
		multiple(a, b);
		System.out.println(result);
		lock.unlock();
	}

	public void multiple(int a, int b) {
		lock.lock();
		result = a * b;
		lock.unlock();
	}
}

class NoReentrantLock {
	public boolean isLocked = false;

	public synchronized void lock() {
		try {
			while (isLocked) {
				wait();
			}
			isLocked = true;
		} catch (Exception e) {

		}
	}

	public synchronized void unlock() {
		try {
			isLocked = false;
			notifyAll();
		} catch (Exception e) {

		}
	}

}
