package org.sky.x.concurrent.thread;

import java.util.concurrent.atomic.AtomicInteger;

public class VolatileTest {

	public static final int THREAD_COUNT = 20;

	public static volatile int race = 0; // volatile可以保证可见行，但不保证唯一性
	public static AtomicInteger race1 = new AtomicInteger(0);

	public static void increace() {
		race++;
	}

	public static void increace1() {
		race1.incrementAndGet();
	}

	public static void main(String[] args) {
		Thread[] threads = new Thread[THREAD_COUNT];
		for (int i = 0; i < THREAD_COUNT; i++) {
			threads[i] = new Thread(new Runnable() {
				public void run() {
					for (int i = 0; i < 10000; i++) {
						increace();
						increace1();
					}
				}
			});
			threads[i].start();
		}
		while (Thread.activeCount() > 1) {
			Thread.yield();
		}
		System.out.println("result:" + race);
		System.out.println("result:" + race1.get());
	}
}

// 乱序执行

