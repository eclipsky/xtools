package org.sky.x.concurrent.atom;

public class AtomicReferenceTest {

	public static void main(String[] args) {
		AtomicReferenceTest demo = new AtomicReferenceTest();
		demo.test();
	}


	public void test() {
		Person p = new Person(10, "annouymous");
		p.print();
		Modifier m1 = new Modifier(p, 2,"sam");
		Modifier m2 = new Modifier(p, 3,"jack");
		new Thread(m2).start();
		new Thread(m1).start();
	}

	class Modifier implements Runnable{

		private Person p;
		private int score;
		private String name;
		public Modifier(Person p, int score, String name){
			this.p = p;
			this.score = score;
			this.name = name;
		}

		@Override
		public void run() {
			p.setPerson(score,name);
		}
	}

	static class Person {
		private int count;
		private String name;

		public Person(int count, String name){
			this.count = count;
			this.name = name;
		}

		public void setPerson(int score, String name){
			this.count = this.count  + score ;
			this.name = name;
			print();
		}

		public void print(){
			System.out.println("count:" + this.count + ", name:" + this.name);
		}
	}
}
