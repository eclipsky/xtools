package org.sky.x.concurrent.atom;

import com.google.common.base.Stopwatch;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AtomicIntegerByMeansTest {
	private AtomicInteger atomicNum = new AtomicInteger(0);
	private int normalNum = 0;
    private int syncNum = 0;
    private int lockNum = 0;
	private Lock lock = new ReentrantLock();
	private static final Unsafe unsafe;
	private int unsafeNum;
	private static final long unsafeNumOffset;
	private CountDownLatch countDownLatch = new CountDownLatch(10);
	static {
		try {
			Field f = Unsafe.class.getDeclaredField("theUnsafe");
			f.setAccessible(true);
			unsafe = (Unsafe)f.get(null);
			unsafeNumOffset =  unsafe.objectFieldOffset(AtomicIntegerByMeansTest.class.getDeclaredField("unsafeNum"));
//			valueOffset = unsafe.objectFieldOffset(AtomicInteger.class.getDeclaredField("value"));
		} catch (Exception e){
			throw new Error(e);
		}
	}

	public void lockAdd() {
		lock.lock();
		lockNum++;
		lock.unlock();
	}

	public void atomicAdd() {
		atomicNum.incrementAndGet();
	}

	public synchronized void syncAdd() {
		syncNum++;
	}

	public void unsafeAdd(){
		while (true){
			int tmp = unsafeNum;
			if(unsafe.compareAndSwapInt(this, unsafeNumOffset, unsafeNum, unsafeNum + 1)){
				return;
			}
		}
	}

	public void normalAdd(){
	    normalNum++;
    }


	public void test() throws InterruptedException {
        Stopwatch stop = Stopwatch.createStarted();
		for (int i = 0;i < 10;i++){
			Thread t = new Thread(() -> {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int i1 = 0; i1 < 100000; i1++) {
//					 atomicAdd();
//					 lockAdd();
					 syncAdd();
//					 unsafeAdd();
//                     normalAdd();
                }
                countDownLatch.countDown();
			});
			t.start();
		}
		countDownLatch.await();

        stop.stop();
		System.out.println("atomicNum: " + atomicNum + ", cost : " + stop);
        System.out.println("lockNum: " + lockNum + ", cost : " + stop);
        System.out.println("syncNum: " + syncNum + ", cost : " + stop);
        System.out.println("unsafeNum: " + unsafeNum + ", cost : " + stop);
        System.out.println("normalNum: " + normalNum + ", cost : " + stop);
	}

	public static void main(String[] args) throws InterruptedException {
		AtomicIntegerByMeansTest test = new AtomicIntegerByMeansTest();
		test.test();
	}
}
