package org.sky.x.concurrent.atom;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class MyAtomicNumber {

    public static AtomicInteger value = new AtomicInteger(0);
    public static ExecutorService executor = Executors.newFixedThreadPool(5);

    public static void main(String[] args) {
        CyclicBarrier barrier = new CyclicBarrier(5, new Runnable() {
            @Override
            public void run() {
                System.out.println("over:" + value);
            }

        });
        Thread t1 = new Increator(barrier);
        Thread t2 = new Increator(barrier);
        Thread t3 = new Increator(barrier);
        Thread t4 = new Increator(barrier);
        Thread t5 = new Increator(barrier);
        executor.execute(t1);
        executor.execute(t2);
        executor.execute(t3);
        executor.execute(t4);
        executor.execute(t5);
        executor.shutdown();
    }

    static class Increator extends Thread {
        private CyclicBarrier barrier;

        public Increator(CyclicBarrier barrier) {
            this.barrier = barrier;
        }

        @Override
        public void run() {
            int last = 0;
            for (int i = 0; i < 1000000; i++) {
                last = value.incrementAndGet();
            }
            try {
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println(last);
        }
    }

}
