package org.sky.x.logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

public class Log4jLogging {
    // load classes/log4j.properties
    public static final Logger logger = Logger.getLogger(Log4jLogging.class);

    public static Log logger2 = LogFactory.getLog(Log4j2Logging.class);

    public static void main(String[] args) {
        logger.error("hello, log4j");
        logger2.info("hello, jcl with " + logger2.getClass());
    }
}
