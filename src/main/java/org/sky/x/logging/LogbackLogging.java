package org.sky.x.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;

public class LogbackLogging {
    // load default config logback.groovy.2, logback-test.xml, logback.xml
    public static Logger logger = LoggerFactory.getLogger(LogbackLogging.class);
    String log = org.slf4j.Logger.ROOT_LOGGER_NAME;


    public static void main(String[] args) {
        logger.info("LogbackLogging");
        // print internal state
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        StatusPrinter.print(lc);
    }
}
