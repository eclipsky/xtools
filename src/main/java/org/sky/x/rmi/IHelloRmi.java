package org.sky.x.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IHelloRmi extends Remote {

	public String hello() throws RemoteException;

	public String say(String name) throws RemoteException;

}
