package org.sky.x.rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class HelloRmiServer {
	public static void main(String[] args) throws Exception {
		// 创建注册表
		Registry registry = LocateRegistry.createRegistry(8866);
		// 创建远程对象
		IHelloRmi hello = new HelloRmiImpl();
		// 绑定远程对象
		registry.rebind("hello", hello);
		System.out.println("远程对象绑定成功 -> " + hello);
	}
}
