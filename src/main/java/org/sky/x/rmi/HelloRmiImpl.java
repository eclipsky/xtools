package org.sky.x.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class HelloRmiImpl extends UnicastRemoteObject implements IHelloRmi {

	private static final long serialVersionUID = 541312883006549825L;

	protected HelloRmiImpl() throws RemoteException {
		super();
	}

	@Override
	public String hello() throws RemoteException {
		return "hello world";
	}

	@Override
	public String say(String name) throws RemoteException {
		return "hi, " + name;
	}

}
