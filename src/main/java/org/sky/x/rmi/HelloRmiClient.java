package org.sky.x.rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class HelloRmiClient {
	public static void main(String[] args) throws Exception {
		// 获取注册表
		Registry registry = LocateRegistry.getRegistry("localhost", 8866);
		String[] list = registry.list();
		for (String item : list) {
			System.out.println("registry: " + item);
		}
		IHelloRmi hello = (IHelloRmi) registry.lookup("hello");
		System.out.println(hello.hello());
		System.out.println(hello.say("sam"));
	}
}
