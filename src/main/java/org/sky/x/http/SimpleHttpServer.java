package org.sky.x.http;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

/**
 * simple http server in jdk
 */
public class SimpleHttpServer {
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(10086), 0);
        server.createContext("/test", new TestHandler());
        server.createContext("/authenticate", new TestHandler());
        server.createContext("/authorize", new TestHandler());
        server.start();
    }

    static class TestHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange){
            String response = "";

            try{
                //获得查询字符串(get)
                String queryString =  httpExchange.getRequestURI().getQuery();
                Map<String,String> queryStringInfo = formData2Dic(queryString);
                System.out.println("query: " + queryStringInfo);
                //获得表单提交数据(post)
                String postString = IOUtils.toString(httpExchange.getRequestBody());
                Map<String,String> postInfo = formData2Dic(postString);
                System.out.println("body: " + postInfo);

                httpExchange.sendResponseHeaders(200,0);
                OutputStream os = httpExchange.getResponseBody();
                os.write(response.getBytes());
                os.close();
            }catch (IOException ie) {

            } catch (Exception e) {

            }
        }
    }

    public static Map<String,String> formData2Dic(String formData ) {
        Map<String,String> result = new HashMap<>();
        if(formData== null || formData.trim().length() == 0) {
            return result;
        }
        final String[] items = formData.split("&");
        Arrays.stream(items).forEach(item ->{
            final String[] keyAndVal = item.split("=");
            if( keyAndVal.length == 2) {
                try{
                    final String key = URLDecoder.decode( keyAndVal[0],"utf8");
                    final String val = URLDecoder.decode( keyAndVal[1],"utf8");
                    result.put(key,val);
                }catch (UnsupportedEncodingException e) {}
            }
        });
        return result;
    }
}
