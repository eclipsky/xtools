package org.sky.x.jdk8;

import java.util.ArrayList;
import java.util.List;

public class LambdaDemo {
	public static void main(String[] args) {
		List<String> names = new ArrayList<String>();
		names.add("sam");
		names.forEach(name -> System.out.println(name));
	}
}
