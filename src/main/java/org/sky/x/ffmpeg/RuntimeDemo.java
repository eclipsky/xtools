package org.sky.x.ffmpeg;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RuntimeDemo {

    private static String _CMD_FFMPEG = "/usr/local/bin/ffmpeg";
    private static String _CMD_FFPROBE = "/usr/local/bin/ffprobe";
    private static String _VIDEO = "/Users/sam/Documents/workspace/ffmpeg/black.mp4";

    static {
        String os = System.getProperty("os.name").toLowerCase();
        String cmd = _CMD_FFMPEG;
        if(os.contains("windows")){
            cmd = cmd + ".exe";
        }
    }

    /**
     * 分割出来的问题差异较大，why？
     * -ss -to -t的有效组合？
     *
     */
    @Test
    public void split(){

        String args = " -y -i " + _VIDEO + " -t 00:00:10 -c copy /tmp/part1.mp4 -ss 00:00:10 -to 00:00:20 -c copy /tmp/part2.mp4 -ss 00:00:20 -c copy /tmp/part3.mp4";
        try{
//            Process process = Runtime.getRuntime().exec(cmd);
            Process process;
            process = Runtime.getRuntime().exec(_CMD_FFMPEG + args);
            process = new ProcessBuilder().command(_CMD_FFMPEG + args).start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
//            BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(process.getInputStream())));
            String line;
            while((line = reader.readLine()) != null){
                System.out.println(line);
            }
            process.waitFor();
            if (process.exitValue() != 0) {
                System.out.println("error! exitCode -> " + process.exitValue());
            }
//            reader.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void merge(){
//        String
    }
}
