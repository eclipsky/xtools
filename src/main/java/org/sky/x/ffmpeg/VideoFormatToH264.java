package org.sky.x.ffmpeg;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * @author: YiRui_Dai
 * @create: 2019/10/10 16:18
 * @description:
 */
public class VideoFormatToH264 {

    private static boolean IS_START = true;

    /**
     * 获取视频宽高、时长
     *
     * @param filePath
     * @return
     * @throws FrameGrabber.Exception
     */
    public static FFmpegVideo getVideoInfo(String filePath) throws FrameGrabber.Exception {
        FFmpegFrameGrabber ff = FFmpegFrameGrabber.createDefault(filePath);
        ff.start();
        double duration = ff.getLengthInTime() / (1000 * 1000.0);
        FFmpegVideo ffmpegVideo = new FFmpegVideo(ff.getImageWidth(), ff.getImageHeight(), duration, ff.getFrameRate(), ff.getVideoBitrate(),ff.getVideoCodec());
        ff.stop();
        return ffmpegVideo;
    }

    public static boolean frameRecord(String inputPath, String outputPath) {
        try {
            FFmpegVideo ffmpegVideo = getVideoInfo(inputPath);
            // 获取视频源
            FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(inputPath);
            if (avcodec.AV_CODEC_ID_H264 == ffmpegVideo.getVideoCodec()) {
                return false;
            }
            // 流媒体输出地址，分辨率（长，高），是否录制音频（0:不录制/1:录制）
            FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(outputPath, ffmpegVideo.getWidth(), ffmpegVideo.getHeight(), 1);
            recorder.setFrameRate(ffmpegVideo.getFrameRate());
            recorder.setVideoBitrate(ffmpegVideo.getVideoBitrate() * 4 / 5);
            //编码格式
            recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
            recordByFrame(grabber, recorder);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void recordByFrame(FFmpegFrameGrabber grabber, FFmpegFrameRecorder recorder)
            throws Exception {
        try {// 建议在线程中使用该方法
            grabber.start();
            recorder.start();
            long t1 = System.currentTimeMillis();
            Frame frame;
            while (IS_START && (frame = grabber.grabFrame()) != null) {
                long t2 = System.currentTimeMillis();
                if (t2 - t1 > 2 * 60 * 60 * 1000) {
                    break;
                } else {
                    recorder.record(frame);
                }
            }
            recorder.stop();
            grabber.stop();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (grabber != null) {
                grabber.stop();
            }
        }
    }

    public static void main(String[] args) {
        String inputFilePth = "D:\\video\\javacv\\boss.avi";
        String outputFilePath = "D:\\video\\javacv\\";
        String name = "boss.mp4";
        ExecutorService executor = Executors.newCachedThreadPool();
        try {
            for(int i=0;i<20;i++){
                int idx = i;
                executor.submit(() -> {
                    frameRecord(inputFilePth, outputFilePath + idx + "-" + name);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class FFmpegVideo {
        public Integer getWidth() {
            return width;
        }

        public void setWidth(Integer width) {
            this.width = width;
        }

        public Integer getHeight() {
            return height;
        }

        public void setHeight(Integer height) {
            this.height = height;
        }

        public Double getDuration() {
            return duration;
        }

        public void setDuration(Double duration) {
            this.duration = duration;
        }

        public Double getFrameRate() {
            return frameRate;
        }

        public void setFrameRate(Double frameRate) {
            this.frameRate = frameRate;
        }

        public Integer getVideoBitrate() {
            return videoBitrate;
        }

        public void setVideoBitrate(Integer videoBitrate) {
            this.videoBitrate = videoBitrate;
        }

        public Integer getVideoCodec() {
            return videoCodec;
        }

        public void setVideoCodec(Integer videoCodec) {
            this.videoCodec = videoCodec;
        }

        private Integer width;
        private Integer height;
        private Double duration;
        private Double frameRate;
        private Integer videoBitrate;
        private Integer videoCodec;

        public FFmpegVideo() {

        }

        public FFmpegVideo(Integer width, Integer height, Double duration, Double frameRate, Integer videoBitrate, Integer videoCodec) {
            this.width = width;
            this.height = height;
            this.duration = duration;
            this.frameRate = frameRate;
            this.videoBitrate = videoBitrate;
            this.videoCodec = videoCodec;
        }
    }
}
