package org.sky.x.ffmpeg;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.probe.FFmpegFormat;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import net.bramp.ffmpeg.probe.FFmpegStream;
import org.junit.Test;

/**
 * https://github.com/bramp/ffmpeg-cli-wrapper
 */
public class FfmpegCliWrapperDemo {

    @Test
    public void getInfo() throws Exception {
        String path = "/usr/local/bin/";
        FFmpeg ffmpeg = new FFmpeg(path + "/ffmpeg");
        String videoPath = "/Users/sam/Documents/workspace/ffmpeg/black.mp4"; //d:\xianxia.mp4
        FFprobe ffprobe = new FFprobe(path + "/ffprobe");
        FFmpegProbeResult probeResult = ffprobe.probe("");

        FFmpegFormat format = probeResult.getFormat();
        System.out.format("%nFile: '%s' ; Format: '%s' ; Duration: %.3fs",
                format.filename,
                format.format_long_name,
                format.duration
        );

        FFmpegStream stream = probeResult.getStreams().get(0);
        System.out.format("%nCodec: '%s' ; Width: %dpx ; Height: %dpx",
                stream.codec_long_name,
                stream.width,
                stream.height
        );
    }


    @Test
    public void split() throws Exception {

        FFmpeg ffmpeg = new FFmpeg("D:\\itools\\ffmpeg\\bin\\ffmpeg.exe");
        FFprobe ffprobe = new FFprobe("D:\\itools\\ffmpeg\\bin\\ffprobe.exe");
        FFmpegProbeResult probeResult = ffprobe.probe("d:\\xianxia.mp4");

        FFmpegBuilder builder = new FFmpegBuilder().setInput(probeResult);
    }

}
