package org.sky.x.exception;

public enum ExceptionEnum {
	REDIRECT_302("Cookie失效:302跳转"), //
	REDIRECT_401("Cookie失效:401未授权"), //
	COOKIE_EXPIRE("Cookie失效"), //
	COOKIE_NOT_EXISTS("Cookie不存在"), //
	SKEY_NOT_EXISTS("Skey不存在"), //
	SERVER_ERROR("服务器错误"), //
	REQUEST_ERROR("请求错误"), //
	;

	private ExceptionEnum(String msg) {
		this.msg = msg;
	}

	private String msg;

	public String getMsg() {
		return msg;
	}
}
