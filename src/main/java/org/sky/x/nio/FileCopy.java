package org.sky.x.nio;

import org.junit.Test;

import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

public class FileCopy {
    @Test
    public void copy() throws Exception{
        RandomAccessFile fileFrom = new RandomAccessFile("/tmp/mydata", "rw");
        FileChannel channlFrom = fileFrom.getChannel();

        RandomAccessFile fileTo = new RandomAccessFile("/tmp/mydata.to", "rw");
        FileChannel channelTo = fileTo.getChannel();

        channlFrom.transferTo(0, channlFrom.size(), channelTo);
        channlFrom.close();
        channelTo.close();
    }
}