package org.sky.x.nio;

import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class SelectorDemo {

    public static void main(String[] args) {

    }

    @Test
    public void server() throws Exception {
        Selector selector = Selector.open();
        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.bind(new InetSocketAddress("127.0.0.1", 8888));
        ssc.configureBlocking(false);
        ssc.register(selector, SelectionKey.OP_ACCEPT);
        System.out.println("server started...");
        Thread t = new Thread(new SelectorTask(selector));
        t.run();
    }

    @Test
    public void client() throws IOException {
        SocketChannel sc = SocketChannel.open();
        sc.connect(new InetSocketAddress("127.0.0.1", 8888));
        String msg = "hello, my little fellow";
        ByteBuffer buffer = ByteBuffer.allocate(64);
//        while(true){
        buffer.clear();
        // buffer write
        buffer.put(msg.getBytes());

        // buffer read
        buffer.flip();
        sc.write(buffer);

        // buffer write
        buffer.clear();
        sc.read(buffer);
        buffer.flip();
        while (buffer.hasRemaining()) {
            System.out.print((char) buffer.get());
        }
        System.out.println();
        sc.close();
//      }

        // TODO client主动退出，在server端会触发一次OP_READ操作，程序自动退出也会触发close操作？
        // https://blog.csdn.net/u010013573/article/details/86485856?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase
    }

    class SelectorTask implements Runnable {
        private Selector selector;

        public SelectorTask(Selector selector) {
            this.selector = selector;
        }

        @Override
        public void run() {
            while (true) {
                try {

                    int readyChannels = selector.select();
                    if (readyChannels == 0) continue;
                    System.out.println("readyChannels: " + readyChannels);
                    Set<SelectionKey> keys = selector.selectedKeys();
                    Iterator<SelectionKey> iterator = keys.iterator();
                    while (iterator.hasNext()) {
                        SelectionKey key = iterator.next();
                        if (key.isAcceptable()) {
                            SocketChannel sc = ((ServerSocketChannel) key.channel()).accept();
                            if (null != sc) {
                                System.out.println("accept client -> " + sc.getRemoteAddress());
                                sc.configureBlocking(false);
                                //  | SelectionKey.OP_WRITE，什么情况才是isWritable？
                                sc.register(selector, SelectionKey.OP_READ);
                            }
                        }
                        if (key.isReadable()) {
                            System.out.println("reading...");
                            ByteBuffer buffer = ByteBuffer.allocate(64);
                            SocketChannel sc = (SocketChannel) key.channel();
                            sc.read(buffer);
                            if(buffer.limit()==1){
                                continue;
                            }
                            buffer.flip();
                            while (buffer.hasRemaining()) {
                                System.out.print((char) buffer.get());
                            }
                            System.out.println();
                            buffer.clear();
                            buffer.put("i'm fine, and you?".getBytes());
                            buffer.flip();
                            sc.write(buffer);
                        }
                        iterator.remove();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
