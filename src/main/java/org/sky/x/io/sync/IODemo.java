package org.sky.x.io.sync;

import java.io.*;

public class IODemo {

    public void testInputStream() throws FileNotFoundException {
        BufferedInputStream is= new BufferedInputStream(new FileInputStream(""));

        char[] chars = new char[1024];
        Reader reader = new CharArrayReader(chars);
    }

    public static void main(String[] args) throws IOException {
        char a;
        while ((a = (char)System.in.read()) != 'd'){
            System.out.println(a);
        }
    }
}
