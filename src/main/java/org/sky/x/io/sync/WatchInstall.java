package org.sky.x.io.sync;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WatchInstall {
	private final WatchService watcher;
	private final Map<WatchKey, Path> keys;
	private final boolean recursive;
	private boolean trace = false;
	@SuppressWarnings("unused")
	private Path rootDir, tmp = null;

	@SuppressWarnings("unchecked")
	static <T> WatchEvent<T> cast(WatchEvent<?> event) {
		return (WatchEvent<T>) event;
	}

	static void usage() {
		System.err.println("usage: java WatchInstall [-r] watchDir");
		System.exit(-1);
	}

	/**
	 * 创建监视服务，使用监视目录注册到该服务上
	 */
	WatchInstall(Path dir, boolean recursive) throws IOException {
		this.rootDir = dir;
		this.watcher = FileSystems.getDefault().newWatchService();
		// System.out.println(FileSystems.getDefault().getClass().getName());
		// System.out.println(watcher.getClass().getName());
		this.keys = new HashMap<WatchKey, Path>();
		this.recursive = recursive;

		if (recursive) {
			System.out.format("Scanning %s ...\n", dir);
			registerAll(dir);
			System.out.println("Done.");
		} else {
			register(dir);
		}
		this.trace = true;
	}

	/**
	 * 注册给定的目录到监视服务
	 */
	private void register(Path dir) throws IOException {
		WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE,
				ENTRY_MODIFY);
		// 这里应该保留的是文件系统句柄，就算是重命名，文件句柄不会变，所以对应的key还是已有的，以此来判断是否重命名。
		if (trace) {
			Path existing = keys.get(key);
			if (existing == null) {
				System.out.format("register: %s\n", dir);
			} else {
				if (!dir.equals(existing)) {
					System.out.format("update: %s -> %s\n", existing, dir);
				}
			}
		}
		keys.put(key, dir);
	}

	/**
	 * 注册目录及其子目录到监视服务
	 */
	private void registerAll(final Path start) throws IOException {
		
		Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir,
					BasicFileAttributes attrs) throws IOException {
				register(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	@SuppressWarnings("unchecked")
	void processEvents() {
		for (;;) {
			// 等待监视事情发生
			WatchKey key;
			try {
				key = watcher.take();
				// System.out.println(key.getClass().getName());
			} catch (InterruptedException x) {
				return;
			}
			Path path = keys.get(key);
			if (path == null) {
				continue;
			}
			for (WatchEvent<?> event : key.pollEvents()) {
				WatchEvent.Kind kind = event.kind();
				if (kind == OVERFLOW) {
					continue;
				}
				// 目录监视事件的上下文是文件名
				WatchEvent<Path> evt = cast(event);
				Path name = evt.context();
				Path child = path.resolve(name);
				System.out.format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new Date())
						+ "  %s|%s\n", event.kind().name(), child);
				// 递归地注册到监视服务
					if (recursive && (kind == ENTRY_CREATE)) {
					try {
						if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
							registerAll(child);
						}
					} catch (IOException x) {
					}
				}
			}
			// 重置key
			boolean valid = key.reset();
			if (!valid) {
				keys.remove(key);
				if (keys.isEmpty()) {
					break;
				}
			}
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
			if (args.length == 0 || args.length > 2)
			usage();
		boolean recursive = false;
		int dirArg = 0;
		if (args[0].equals("-r")) {
			if (args.length < 2)
				usage();
			recursive = true;
			dirArg++;
		}
		Path dir = Paths.get(args[dirArg]);
		recursive = true;
		new WatchInstall(dir, recursive).processEvents();
	}

}
