package org.sky.x.io.sync;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class BackupInstall {
	private String times = new SimpleDateFormat("yyyyMMdd_hh_mm")
			.format(new Date());
	private String rootDir[] = null;

	public void copy(Path rootDir, Path source, CopyOption option) {
		Path relativePath = source.subpath(rootDir.getNameCount(), source
				.getNameCount());
		Path backDir = rootDir.getParent().resolve(
				rootDir.getFileName() + "_backup_" + times);
		Path target = backDir.resolve(relativePath);
		
		System.out.println("rootdir=" + rootDir + " source=" + source
				+ "  target=" + target);
		if (Files.notExists(target.getParent())) {
			try {
				Files.createDirectories(target.getParent());
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		if (Files.exists(target)) {

		} else
			try {

				Files.copy(source, target, option);
			} catch (IOException e) {

				e.printStackTrace();
			}
	}

	public void ReadAndCopy(Path p) {
		HashMap<Path, String> map = new HashMap<Path, String>();
		InputStream inputStream;
		try {
			inputStream = Files.newInputStream(p);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputStream));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("Scanning")) {
					rootDir = line.split(" ");
				}

				if (line.startsWith("register") || line.startsWith("update")
						|| line.startsWith("Scanning")
						|| line.startsWith("Done")
						|| line.indexOf("ENTRY_DELETE") > 0) {
					continue;
				}

				line = line.substring(line.indexOf("|") + 1);
				if (Files.notExists(Paths.get(line))
						|| map.containsKey(Paths.get(line))) {
					continue;
				}

				map.put(Paths.get(line), "test");
				copy(Paths.get(rootDir[1]), Paths.get(line),
						REPLACE_EXISTING);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			new BackupInstall().ReadAndCopy(Paths
					.get("c:\\watchinstall.txt"));
		} else {
			new BackupInstall().ReadAndCopy(Paths.get(args[0]));
		}
		System.exit(0);
	}
}
