package org.sky.x.io.sync;

import java.io.IOException;
import java.nio.file.FileSystems;

import static java.nio.file.StandardWatchEventKinds.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

/**
 * 监控文件变化
 * 
 * @author sam.xie
 * @date Mar 23, 2017 8:01:47 PM
 * @version 1.0
 */
public class FileWatcher {

	private WatchService watcher;

	public FileWatcher() throws IOException {
		watcher = FileSystems.getDefault().newWatchService();
	}

	public void register(Path path) throws IOException {
		path.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
	}

	public void monitor() throws InterruptedException {
		while (true) {
			WatchKey key = watcher.take();
			for (WatchEvent<?> event : key.pollEvents()) {
				WatchEvent<Path> evt = (WatchEvent<Path>) (event);
				Kind<Path> kind = evt.kind();
				if (kind.equals(OVERFLOW)) {
					continue;
				}
				Path path = evt.context();
				System.out.println(kind.name() + "|" + path);
				// updateOnChange(evt.context().);
			}
			key.reset();
		}
	}

	public void updateOnChange() {
		System.out.println();

	}

	public static void main(String[] args) throws IOException, InterruptedException {
		FileWatcher watcher = new FileWatcher();
		watcher.register(Paths.get("/tmp/01"));
		watcher.register(Paths.get("/tmp/02"));
		watcher.monitor();
	}
}
