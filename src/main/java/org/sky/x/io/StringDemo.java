package org.sky.x.io;

import java.awt.*;

public class StringDemo {
    // 直接String a = new String("XXX"), a = "XXX";
    public static void main(String[] args) {
        //String name0 = "sam" + "aras";
        String name9 = "hello1";
        String name0 = new String("hello1");
        String name1 = new String("hello1");
        String name2 = new String(name1);
        String name3 = name1;
//        System.out.println(name1 == "sam");
        System.out.println(name1 == name2);
        System.out.println(name1 == name3);

        Point p1 = new Point();
        Point p2 = new Point();
        Point[] arr1 = new Point[]{p1, p2};
        Point[] arr2 = new Point[2];
        System.arraycopy(arr1, 0, arr2, 0 ,2);
        System.out.println(arr1);
        System.out.println(arr2);
    }
}
