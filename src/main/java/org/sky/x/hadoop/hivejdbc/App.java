package org.sky.x.hadoop.hivejdbc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App {

	public static void mysqlJDBC() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		FileOutputStream fos = new FileOutputStream("d:\\t.tmp");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeInt(12345);
		oos.writeObject("Today");
		oos.writeObject(new Date());

		oos.close();

		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("d:\\t.tmp"));
		System.out.println(ois.readInt());
		System.out.println(ois.readObject());
		System.out.println(ois.readObject());
		System.out.println(ois.readObject());
	}

	public static void hiveJDBC() throws Exception {
		Class.forName("org.apache.hive.jdbc.HiveDriver");
		Connection conn;
		Statement stat;
		ResultSet result;
		try {
			conn = DriverManager.getConnection("jdbc:hive2://slave1:10000");
			stat = conn.createStatement();
			result = stat.executeQuery("show databases");
			while (result.next()) {
				System.out.println(result.getString(1));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void impalaJDBC() throws Exception {
		Class.forName("org.apache.hive.jdbc.HiveDriver");
		Connection conn;
		Statement stat;
		ResultSet result;
		try {
			conn = DriverManager.getConnection("jdbc:hive2://slave2:21050");
			stat = conn.createStatement();
			result = stat.executeQuery("show databases");
			while (result.next()) {
				System.out.println(result.getString(1));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static void main(String[] args) throws Exception {
//		hiveJDBC();
		impalaJDBC();
	}
}
