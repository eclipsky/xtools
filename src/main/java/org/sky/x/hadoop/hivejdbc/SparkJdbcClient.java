package org.sky.x.hadoop.hivejdbc;

import java.sql.*;
import java.util.Random;

public class SparkJdbcClient {
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";
    private static String jdbcUrl = "jdbc:hive2://localhost:9990/default;auth=none";

    //    private static String driverName = "com.cloudera.impala.jdbc41.Driver";
    //    	private static String jdbcUrl = "jdbc:impala://slave2:21050/default;auth=noSasl";

    /**
     * @param args
     * @throws SQLException
     */
    public static void main(String[] args) throws SQLException {
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);
        }
        singleThread();
//        multiThread();
    }

    public static void singleThread(){
        Thread t1 = new Thread(new SparkClient("sam.xie@sinaif.com", "Sam@111111"), "user01");
        t1.start();
    }

    private static void multiThread(){
        Random rand = new Random();
        int seed = rand.nextInt(10);
        int i = 1;
        while(true) {
            int no = 10000 + i;
            Thread t1 = new Thread(new SparkClient("user_" + no, "pass_" + no), "user_" + no);
            t1.start();
            try{
                Thread.sleep(seed * 1000);
            }catch (Exception e){
                e.printStackTrace();
            }
            i++;
        }

    }

    static class SparkClient implements Runnable {

        private String user;
        private String password;
        private Connection conn;
        private Statement stmt;

        public SparkClient(String user, String password) {
            this.user = user;
            this.password = password;
            try {
                conn = DriverManager.getConnection(jdbcUrl, user, password);
                stmt = conn.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            try {
                String tableName = "t_test_" + user;
                int times = 1;
//                while (times <= 10) {
                    System.out.println("----------------------------------------" + times + "\t" + user);
//                    execute("drop table if exists " + tableName);
//                    execute("drop table " + tableName);
//                    execute("create table if not exists " + tableName + " (key int, value string)");
//                    execute("insert into table " + tableName + " values (1, 'sam'),(2, 'eva')");
//                    executeQuery("select key, value from " + tableName);
                    executeQuery("show databases");
                    times++;
                    Thread.sleep(1000 * 10);
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void execute(String sql) {
            try {
                System.out.println("execting sql: " + sql);
                stmt.execute(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        private void executeQuery(String sql) {
            try {
                System.out.println("execting sql: " + sql);
                ResultSet res = stmt.executeQuery(sql);
                while (res.next()) {
                    System.out.println(String.valueOf(res.getString(1)));
//                    System.out.println(String.valueOf(res.getInt(1)) + "\t" + res.getString(2));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
