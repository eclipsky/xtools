package org.sky.x.sql;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.ShowStatement;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.UseStatement;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.update.Update;
import net.sf.jsqlparser.util.TablesNamesFinder;

public class SqlParser {

    public static  final Pattern  _PATTERN_SHOW_CREATE_TABLE = Pattern.compile("^show\\s+create\\s+table\\s+([a-z,A-z]+[a-zA-Z0-9_.]*)");
//    public static  final Pattern  _PATTERN_SHOW_CREATE_TABLE = Pattern.compile("show\\s+create\\s+table\\s+");
    public static final Pattern  _PATTERN_SHOW_DATABASES = Pattern.compile("^show\\s+databases$");
    public static final String multiSql = "select current_database(); use db1; show databases; show create table t_user_info; select count(1) from db1.my_table1 t11 inner join db2.my_table2 t22 on t11.id = t22.id; delete from db2.my_table2";

    public static void main(String[] args) throws JSQLParserException {
//        String sql = "show databases";
//        Matcher matcher = _PATTERN_SHOW_DATABASES.matcher(sql);
//        if(matcher.find()){
//            System.out.println(matcher.group(0));
//        }
        parse(multiSql);
    }

    public static void parse(String multiSql){
    //      Statements statements = CCJSqlParserUtil.parseStatements(multiSql);
            String[] sqls = multiSql.split(";");
            for (String sql : sqls) {
                sql = sql.trim();
                System.out.println("| " + sql);
                Matcher matcher;
                if((matcher = _PATTERN_SHOW_CREATE_TABLE.matcher(sql)).find()){
                    System.out.println(matcher.group(1));
                }else if(_PATTERN_SHOW_DATABASES.matcher(sql).find()){
                    System.out.println(sql);
                }else{
                    try{
                        Statement statement = CCJSqlParserUtil.parse(sql);
                        if(statement instanceof Select){
                            Select selectStatement = (Select) statement;
                            TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
                            List<String> tableList = tablesNamesFinder.getTableList(selectStatement);
                            tableList.forEach(str -> System.out.println(str));
                        }else if(statement instanceof Update){

                        }else if(statement instanceof Delete){
                            System.out.println(((Delete)statement).getTable());
                        }else if(statement instanceof ShowStatement){







                        }else if(statement instanceof UseStatement){
                            System.out.println(((UseStatement)statement).getName());
                        }else {
                            System.out.println(statement.toString());
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
}
