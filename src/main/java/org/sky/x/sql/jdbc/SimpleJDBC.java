package org.sky.x.sql.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SimpleJDBC {

	private static String url = "jdbc:mysql://192.168.0.110:3306/test?useUnicode=true&amp;characterEncoding=utf8mb4&amp;allowMultiQueries=true";
	private static String user = "root";
	private static String password = "123456";
	private static Connection conn = null;
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void query() throws Exception {
		PreparedStatement stat = null;
		ResultSet rs = null;
		String str1 = "铜厂乡";
		String str2 = "铜\uD840\uDC86乡";
		String sql = "insert into test.clue values (?, ?)";
		try {
			stat = conn.prepareStatement(sql);
//			stat.execute("set names utf8mb4;");
			stat.setString(1, str1);
			stat.setString(2, str2);
			stat.execute();
			rs = stat.executeQuery("select * from test.clue");
			while (rs.next()) {
				String ip = rs.getString("address_utf8");
				String port = rs.getString("address_utf8mb4");
				System.out.println(ip + ":" + port);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			stat.close();
		}
	}

	public static void main(String[] args) throws Exception {
		query();
	}
}
