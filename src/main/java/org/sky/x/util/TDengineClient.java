package org.sky.x.util;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TDengineClient {

    private static HikariDataSource dataSource;

    private static JdbcTemplate jdbcTemplate;

    static {
        init();
    }

    private static void init() {
        try {
            Class.forName("com.taosdata.jdbc.rs.RestfulDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        HikariConfig config = new HikariConfig();
        // jdbc properties
        config.setJdbcUrl("jdbc:TAOS-RS://192.168.0.110:6041/mars");
        config.setUsername("root");
        config.setPassword("taosdata");
        // connection pool configurations
        config.setMinimumIdle(10);           //minimum number of idle connection
        config.setMaximumPoolSize(10);      //maximum number of connection in the pool
        config.setConnectionTimeout(30000); //maximum wait milliseconds for get connection from pool
        config.setMaxLifetime(0);       // maximum life time for each connection
        config.setIdleTimeout(0);       // max idle time for recycle idle connection
        config.setConnectionTestQuery("select server_status()"); //validation query
        dataSource = new HikariDataSource(config); //create datasource

        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public static void main(String[] args) throws SQLException {
//        jdbcByDirect();
        jdbcByTemplate();
    }

    public static void jdbcByDirect(){
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.createStatement();
//            rs = stat.executeQuery("select douyin_acc, live_no from live_comment_001");
//            while(rs.next()){
//                String douyinAcc = rs.getString("douyin_acc");
//                String liveNo = rs.getString("live_no");
//                System.out.println(douyinAcc + " -> " + liveNo);
//            }
            for (int i = 0; i< 1000; i++){
                String name = "我是-" + i;
                stat.execute("insert into mars.live_comment_001 values (now, '" + name + "', '刚进直播，请多关照')");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                if(null != rs){
                    rs.close();
                }
                if(null != stat){
                    stat.close();
                }
                if(null != conn){
                    conn.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private static void jdbcByTemplate(){
        ExecutorService executor = Executors.newFixedThreadPool(64);
        for (int i = 0; i< 20; i++){
            int idx = i;
            executor.execute(() ->{
                String ts = DateUtil.currentDate("yyyy-MM-dd HH:mm:ss.SSS");
                ts = "2021-03-04 19:53:50.143";
                String name = "template-" + idx;
                String sql = "insert into mars.live_comment_001 values ('" + ts + "', '" + name + "', '刚进直播，请多关照')";
                jdbcTemplate.execute(sql);
                System.err.println(idx + "->" + sql);
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            });

        }

    }

}
