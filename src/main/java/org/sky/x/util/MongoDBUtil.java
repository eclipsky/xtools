package org.sky.x.util;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.MongoWriteException;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.WriteModel;

public class MongoDBUtil {
    //    public static String _HOST = "192.168.1.166";
    public static String _HOST = "192.168.1.164";
    public static int _PORT = 27017;
    public static String _USER = "test";
    public static String _PASS = "123456";
    public static String _DB = "test";

    public static MongoClient mongoClient;

    public static MongoDatabase mongoDB;
    public static MongoCollection mongoCol;

    static {
        init();
    }

    private static void init() {
        //连接到MongoDB服务 如果是远程连接可以替换“localhost”为服务器所在IP地址
        //ServerAddress()两个参数分别为 服务器地址 和 端口
        ServerAddress serverAddress = new ServerAddress(_HOST, _PORT);
        List<ServerAddress> addrs = new ArrayList<ServerAddress>();
        addrs.add(serverAddress);

        //MongoCredential.createScramSha1Credential()三个参数分别为 用户名 数据库名称 密码
//        MongoCredential credential = MongoCredential.createCredential(_USER, _DB, _PASS.toCharArray());
//        List<MongoCredential> credentials = new ArrayList<>();
//        credentials.add(credential);
//
//        //通过连接认证获取MongoDB连接
//        mongoClient = new MongoClient(addrs,credentials);
        mongoClient = new MongoClient(addrs);
    }

    private static void query(Bson filter) {

        FindIterable<Document> iter = filter == null ? mongoCol.find() : mongoCol.find(filter);
        MongoCursor<Document> cursor = iter.iterator();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            System.out.println(doc);
        }
    }

//    private static void insert(){
//        Document doc = new Document("status", 10);
//        doc.append("uid", "A0000");
//        mongoCol.insertOne(doc);
//    }


    private static void update() {
        Bson filter = Filters.and(eq("productid", "2001"), eq("userid", "156619719560165293"));
        //指定修改的更新文档
        String uptime = DateUtil.currentDate("yyyy-MM-dd HH:mm:ss");
        Bson update = combine(setOnInsert("crtime", uptime),
                inc("click_stat.append_num", 1),
                set("click_stat.uptime", uptime),
                setOnInsert("click_stat.login_num", 0),
                setOnInsert("uptime", uptime));
        UpdateOptions options = new UpdateOptions();
        mongoCol.updateOne(filter, update, options.upsert(true));
    }

    private static void batchUpdate() {
        List<WriteModel> list = new ArrayList<>();
        list.add(genUpdateOneModel("2001", "176619719560165290", "append_num", 19));
        list.add(genUpdateOneModel("2001", "176619719560165290", "download_num", 12));
        list.add(genUpdateOneModel("2001", "176619719560165290", "append_num", 13));
        list.add(genUpdateOneModel("2001", "176619719560165291", "pageview_num", 14));

        mongoCol.bulkWrite(list);
    }

    private static UpdateOneModel genUpdateOneModel(String productid, String userid, String statItem, int value) {
        Bson filter = Filters.and(eq("productid", productid), eq("userid", userid));
        //指定修改的更新文档
        String uptime = DateUtil.currentDate("yyyy-MM-dd HH:mm:ss");
        Bson update = combine(setOnInsert("crtime", uptime),
                set("click_stat." + statItem, 1),
                set("click_stat.uptime", uptime),
                set("uptime", uptime));
        UpdateOptions options = new UpdateOptions();
        mongoCol.updateOne(filter, update, options.upsert(true));
        return new UpdateOneModel(filter, update, options);
    }

    private static void delete() {

    }


    private static void concurrentInc() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread() + " -> start!");
                    for (int j = 0; j < 100000; j++) {
                        try {

                            System.out.println(Thread.currentThread() + " -> " + j + " -> 1.start");
                            update();
                            System.out.println(Thread.currentThread() + " -> " + j + " -> 2.end");
//                        try {
                            Thread.sleep(new Random().nextInt(100));
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }

                        } catch (MongoWriteException e) {
                            if (e.getMessage().contains("E11000 duplicate key")) {
                                System.out.println(Thread.currentThread() + " -> " + j + " -> retry");
                                update();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    System.out.println(Thread.currentThread() + " -> done!");
                }
            });
        }
//        executor.awaitTermination(1000000, TimeUnit.SECONDS);
    }

    public static void main(String[] args) throws InterruptedException {
        mongoDB = mongoClient.getDatabase("test"); // test
        mongoCol = mongoDB.getCollection("t_predict_eigenvalue"); // t_user
//        mongoCol.createIndex(Indexes.ascending("productid", "userid"), new IndexOptions().unique(true));
//        query(null);
//        insert();

//        concurrentInc();
//        update();
        batchUpdate();
        batchUpdate();
//        Bson filter = eq("userid", "156619719560165295");
//        query(filter);
//        mongoCol.
    }
}
