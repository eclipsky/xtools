package org.sky.x.util;

import org.junit.Test;
import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class UnsafeUtil {
    public static void main(String[] args) throws Exception {
        Unsafe unsafe = getInstance();
        Person person = (Person)unsafe.allocateInstance(Person.class);
        System.out.println(person);
    }

    public static Unsafe getInstance() throws NoSuchFieldException, IllegalAccessException {
        Field f = Unsafe.class.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        Unsafe unsafe = (Unsafe) f.get(null);
        System.out.println(unsafe);
        return unsafe;
    }

    static class Person{
        int age = 10;
        String name = "sam";
        public String toString(){
            return String.format("age:%s, name:%s", age, name);
        }
    }
}
