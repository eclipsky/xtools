package org.sky.x.util;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;

public class Logger {

	private static String HOME_PATH_Property = "HOME_PATH";
	private static String DEFAULT_PATTERN_Property = "DEFAULT_PATTERN";
	private static String Default_Log_Path = "logs/";
	private static String Default_Time_Append = "-%d{yyyy-MM-dd}.log";
	private static String Default_Log_Patten = "%d{yyyy-MM-dd HH:mm:ss.SSS} [%file:%line] [%level] %msg%n";

	private final static ConcurrentHashMap<String, org.slf4j.Logger> loggerMap = new ConcurrentHashMap<String, org.slf4j.Logger>();

	public final static org.slf4j.Logger getLogger(String logName) {
		org.slf4j.Logger logger = loggerMap.get(logName);
		if (logger == null) {
			synchronized (loggerMap) {
				logger = loggerMap.get(logName);
				if (logger == null) {
					logger = createLogger(logName);
					loggerMap.put(logName, logger);
				}
			}
		}
		return logger;
	}

	private final static org.slf4j.Logger createLogger(String logName) {
		ILoggerFactory factory = LoggerFactory.getILoggerFactory();
		LoggerContext context = null;
		try {
			context = (LoggerContext) factory;
		} catch (Throwable t) {
			t.printStackTrace();
			return factory.getLogger(logName);
		}

		String logDir = context.getProperty(HOME_PATH_Property);
		if (logDir == null) {
			logDir = Default_Log_Path;
		}
		String logFileName = logDir + logName + Default_Time_Append;

		// 设置日志格式
		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		encoder.setContext(context);
		// edit by ivan start
		String defaultPattern = context.getProperty(DEFAULT_PATTERN_Property);
		if (defaultPattern == null) {
			defaultPattern = Default_Log_Patten;
		}
		encoder.setPattern(defaultPattern);
		// edit by ivan end

		// 设置time based file naming and triggering policy
		DefaultTimeBasedFileNamingAndTriggeringPolicy<ILoggingEvent> timeBasedTriggeringPolicy = new DefaultTimeBasedFileNamingAndTriggeringPolicy<ILoggingEvent>();
		timeBasedTriggeringPolicy.setContext(context);
		TimeBasedRollingPolicy<ILoggingEvent> timeBasedRollingPolicy = new TimeBasedRollingPolicy<ILoggingEvent>();
		timeBasedRollingPolicy.setContext(context);
		timeBasedRollingPolicy.setMaxHistory(5);
		timeBasedRollingPolicy.setFileNamePattern(logFileName);
		timeBasedRollingPolicy
				.setTimeBasedFileNamingAndTriggeringPolicy(timeBasedTriggeringPolicy);
		timeBasedTriggeringPolicy
				.setTimeBasedRollingPolicy(timeBasedRollingPolicy);

		RollingFileAppender<ILoggingEvent> rollingFileAppender = new RollingFileAppender<ILoggingEvent>();
		rollingFileAppender.setAppend(true);
		rollingFileAppender.setContext(context);
		rollingFileAppender.setEncoder(encoder);
		rollingFileAppender.setName(logName);
		rollingFileAppender.setPrudent(false);
		rollingFileAppender.setRollingPolicy(timeBasedRollingPolicy);
		rollingFileAppender.setTriggeringPolicy(timeBasedTriggeringPolicy);

		// 设置Appender
		timeBasedRollingPolicy.setParent(rollingFileAppender);

		encoder.start();
		timeBasedRollingPolicy.start();

		rollingFileAppender.stop();
		rollingFileAppender.start();

		ch.qos.logback.classic.Logger logbackLogger = (ch.qos.logback.classic.Logger) context
				.getLogger(logName);
		logbackLogger.setAdditive(false);
		logbackLogger.addAppender(rollingFileAppender);
		return logbackLogger;
	}

	public final static void main(String[] args) {
		org.slf4j.Logger logger = Logger.getLogger("test1");
		logger.info("xxxxxxxxxxxx1");
		logger.info("xxxxxxxxxxxx2");
		logger.info("xxxxxxxxxxxx3");
		logger = Logger.getLogger("test2");
		logger.info("xxxxxxxxxxxx1");
		logger.info("xxxxxxxxxxxx2");
		logger.info("xxxxxxxxxxxx3");
		logger = Logger.getLogger("test1");
		logger.info("xxxxxxxxxxxx1");
		logger.info("xxxxxxxxxxxx2");
		logger.info("xxxxxxxxxxxx3");
		for (int i = 0; i < 5; i++) {
			final int x = i;
			new Thread(new Runnable() {
				@Override
				public void run() {
					org.slf4j.Logger logger = Logger.getLogger("test" + x);
					logger.info("aaa=" + x);
				}
			}).start();
		}
	}
}
