package org.sky.x.util;

import java.sql.ResultSet;

public interface ResultHandler {
	void handler(ResultSet resultSet);
}
