package org.sky.x.util;

/**
 * 令牌桶算法，用于限流
 * https://zhuanlan.zhihu.com/p/20872901
 * 基本思想，有一个固定大小的桶（bucket），每隔固定时间加入一个token，如果bucket满了，加入的token会扔掉，
 * 其他地方从bucket里面获取token（api请求，或者网络流量等）
 * 比如，每个用户1秒内最多只能10次请求，1小时内最多10000次请求，这样就需要2个bucket
 * 如果根据IP进行限制，这样bucket就需要根据ip地址来创建
 */
public class TokenBucket {

}
