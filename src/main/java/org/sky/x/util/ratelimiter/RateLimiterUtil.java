package org.sky.x.util.ratelimiter;

import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.RateLimiter;
import org.junit.Test;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <href a="https://cloud.tencent.com/developer/article/1408819"/>
 * Guava RateLimiter使用要注意几点
 * 1、本次获取permit，会影响下次获取permit的等待时间
 * 2、单次请求大量permit，会超过实际限流上限
 *
 * 要实现平稳的QPS控制，可以使用SmoothWarmUp，同时尽量使用acquire(1)
 */
public class RateLimiterUtil {

    @Test
    public void stopWatch() throws InterruptedException {
        Stopwatch stopwatch = Stopwatch.createStarted();
        Thread.sleep(10000);
        stopwatch.stop();
        System.out.println(stopwatch);
        stopwatch.reset();
        stopwatch.start();
        stopwatch.stop();
        System.out.println(stopwatch);
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        RateLimiterUtil test = new RateLimiterUtil();
        test.testRateLimiter();
    }

    @Test
    public void testRateLimiter() throws InterruptedException, IOException {
//        System.out.println(stopwatch.stop());
//        Thread.sleep(1000);
//        System.out.println(stopwatch.start());
//        System.out.println(stopwatch.stop());
        RateLimiter rate = RateLimiter.create(20, 1, TimeUnit.MICROSECONDS);
//        RateLimiter rate = RateLimiter.create(10);
//        System.out.println(System.currentTimeMillis() + " -> started");
//        System.out.println(System.currentTimeMillis() + " -> get 100 token, cost -> " + rate.acquire(100) + "s" + " -> " + System.currentTimeMillis());
        Thread.sleep(2000);
//        System.out.println(System.currentTimeMillis() + " -> get 25 token, cost -> " + rate.acquire(25) + "s" + " -> " + System.currentTimeMillis());
//        System.out.println(System.currentTimeMillis() + " -> get 10 token, cost -> " + rate.acquire(10) + "s" + " -> " + System.currentTimeMillis());
//        System.out.println(System.currentTimeMillis() + " -> get 1 token, cost -> " + rate.acquire(1) + "s" + " -> " + System.currentTimeMillis());
//        System.out.println(System.currentTimeMillis() + " -> get 1 token, cost -> " + rate.acquire(1) + "s" + " -> " + System.currentTimeMillis());
//        System.out.println(System.currentTimeMillis() + " -> get 1 token, cost -> " + rate.acquire(1) + "s" + " -> " + System.currentTimeMillis());
//        System.out.println(System.currentTimeMillis() + " -> get 1 token, cost -> " + rate.acquire(1) + "s" + " -> " + System.currentTimeMillis());
////        Thread.sleep(2000);
        Stopwatch stopwatch = Stopwatch.createStarted();
        System.out.println(stopwatch);
        System.out.println(String.format("current thread -> %s, isDaemon -> %s, active threads -> %s", Thread.currentThread(), Thread.currentThread().isDaemon(), Thread.activeCount()));
        for(int i=0;i<20; i++){
            Thread t = new Thread((() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(System.currentTimeMillis() + " -> get 1 token, cost "+ rate.acquire(1) + "s");
            }));
            t.start();
        }
        System.out.println(String.format("current thread -> %s, isDaemon -> %s, active threads -> %s", Thread.currentThread(), Thread.currentThread().isDaemon(), Thread.activeCount()));
        System.out.println(stopwatch);
        System.out.println("-----------------------------------------");
//        Thread.sleep(2000);
////        rate.setRate(5);
//        for(int i=0;i<20; i++){
//            System.out.println(System.currentTimeMillis() + " -> get 1 token: "+ rate.acquire() + "s");
//        }
//
//        new Timer().schedule(new TimerTask() {
//            @Override
//            public void run() {
//                System.out.println("time task...");
//            }
//        }, 500, 500);
        System.in.read();
//
//        while (Thread.activeCount() > 1) {
//            Thread.sleep(100);
//            System.out.println("active threads -> " + Thread.activeCount());
//        }
    }

    /**
     * 严格满足tps限制
     */

    public void strictRateLimiter(){

    }

    /**
     * 基于syncrhonized的简单限流器
     * 开启token生成器，定时递增token
     * 对于消费者而言，并不限制按固定周期获取token，可以一次性获取全部token，可能会出现1秒内超过tps的情况
     * @throws InterruptedException
     */
    @Test
    public void simpleRateLimiter() throws InterruptedException {
        Thread t1 = new Thread(() -> {
            try {
                tokenProducer();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "tokenProducerThread");
        t1.setDaemon(false);
        t1.start();

        while(true){
            int token = tokenConsumer();
            System.out.println(String.format(">>>>>>>> %s, get token: %s", System.currentTimeMillis(), token));
            Thread.sleep(random.nextInt(1000));
        }
//        while(Thread.activeCount() == 1){
//            System.out.println("game over");
//        }
//        Thread.sleep(1000000);
    }

    private double tps = 5;
    private int NS_PER_SECOND = 1000 * 1000 * 1000;
    private int MS_PER_NAMOSECOND = 1000 * 1000 ;
    private Random random = new Random();
    public AtomicInteger token = new AtomicInteger(0);

    public int tokenConsumer() throws InterruptedException {
        while (true){
            synchronized (token){
                int value = token.get();
                if(value < tps){
                    token.notifyAll();
                }
                if(value <= 0){
                    token.wait();
                }else{
                    value = token.getAndDecrement();
//                    System.out.println(String.format(">>>>>>>> %s, get 1 token -> %s ms", System.currentTimeMillis(), DateUtil.intervalMs(start)));
                    return value;
                }
            }
        }
    }

    public void tokenProducer() throws InterruptedException {
        long waitMs = (long)((double)NS_PER_SECOND/tps / MS_PER_NAMOSECOND);
        int waitNs = (int)((double)NS_PER_SECOND/tps % MS_PER_NAMOSECOND);
        while(true){
            synchronized (token){
                int value = token.get();
                if(value > 0){
                    token.notifyAll();
                }
                if(value < tps){
                    value = token.incrementAndGet();
                    System.out.println(String.format("-------- %s, current token: %s", System.currentTimeMillis(), value));
                }else{
                    System.out.println(String.format("-------- %s, token reach: %s, skip...", System.currentTimeMillis(), value));
                    token.wait();
                }
//                if(value >= tps){
//                    System.out.println("token value reach -> " + value);
//                    token.wait();
//                }else {
//                    token.incrementAndGet();
//                    token.notifyAll();
//                }
            }
            long start = System.currentTimeMillis();
            Thread.sleep(waitMs, waitNs);
        }
    }

    /**
     * 漏桶
     * 桶的大小固定，出桶的速率固定，入桶的数据太快，就会溢出抛弃，
     * 漏桶算法的实现往往依赖于队列，请求到达如果队列未满则直接放入队列，然后有一个处理器按照固定频率从队列头取出请求进行处理。
     * 如果请求量大，则会导致队列满，那么新来的请求就会被抛弃
     */
    class LeakyBucket{

    }

    class TokenBucket{

    }
}


