package org.sky.x.util.ratelimiter;

import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Uninterruptibles;
import org.sky.x.redis.RedisClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * TODO
 * 1、permits设置本地缓存，从redis获取一批许可，减少多次访问redis
 * 2、
 */
public class RedisRateLimiter {
    private static RedisClient client = RedisClient.getInstance();
    private static String _SCRIPT = "/scripts/ratelimiter.lua";
    private static String _SHA_ACQUIRE = "acquire";
    private static String _SHA_CREATE = "create";
    private static String _PERMITS_PER_SECOND = "permitsPerSecond";
    private static String _DEFAULT_SHA1 = "5c07e4008c35786cc581a8b1761265341ded33da";

    private String sha1;
    private String key;
    private Map<String, String> cache;

    private RedisRateLimiter(String sha1, String key, Map<String, String> cache){
        this.sha1 = sha1;
        this.key = key;
        this.cache = cache;
    }

    private static String readLuaScript() throws IOException {
        InputStream is = RedisRateLimiter.class.getResourceAsStream(_SCRIPT);
        byte[] tmp = new byte[128];
        int len;
        List<Byte> list = new ArrayList<>();
        while ((len = is.read(tmp)) != -1){
            for(int i = 0;i < len; i ++){
                list.add(tmp[i]);
            }
        }
        byte[] val = new byte[list.size()];
        for(int i = 0; i< val.length; i++){
            val[i] = list.get(i);
        }
        String result = new String(val);
        return result;
    }

    private long getWaitMicrosFromRedis(int permits){
        Object result = client.evalsha(sha1, Lists.newArrayList(key), Lists.newArrayList(_SHA_ACQUIRE, String.valueOf(permits)));
        return (long)result;
    }

    public static RedisRateLimiter create(String key, int permitsPerSecond) throws IOException {
        return create(_DEFAULT_SHA1, key, permitsPerSecond);
    }

    public static RedisRateLimiter create(String sha1, String key, int permitsPerSecond) throws IOException {
        // 检查sha1对应的脚本是否存在
        if(!client.scriptExists(sha1)){
            // throw new RuntimeException("sha1 not exists, please check -> " + sha1);
            // 读取lua脚本初始化
            System.out.println("read latest scripts from file: " + _SCRIPT);
            String script = readLuaScript();
            String newSha1 = client.scriptLoad(script);
            System.out.println("script new sha1: " + newSha1);
            sha1 = newSha1;
        }
        // 检查redis限流key是否存在，且permitsPerSecond一致
        Map<String, String> cache = client.hgetAll(key);
        if(null == cache || !String.valueOf(permitsPerSecond).equals(cache.get(_PERMITS_PER_SECOND))){
            List<String> result = (List)client.evalsha(sha1, Lists.newArrayList(key), Lists.newArrayList(_SHA_CREATE, String.valueOf(permitsPerSecond)));
            int idx = 0;
            while(idx < result.size()){
                cache.put(result.get(idx++), result.get(idx++));
            }
        }
        System.out.println("RateLimiter Info -> " + cache);
        return new RedisRateLimiter(sha1, key, cache);
    }

    public /*synchronized */double acquire(int permits){
        long waitMicros = getWaitMicrosFromRedis(permits);
        sleepMicrosUninterruptibly(waitMicros);
        return ((double)waitMicros)/1000000;
    }

    private void sleepMicrosUninterruptibly(long micros) {
        if (micros > 0L) {
            Uninterruptibles.sleepUninterruptibly(micros, TimeUnit.MICROSECONDS);
        }
    }
}
