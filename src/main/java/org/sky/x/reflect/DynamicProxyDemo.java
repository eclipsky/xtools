package org.sky.x.reflect;

/**
 * <pre/>
 * 动态代理示例
 * 
 */
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 */
public class DynamicProxyDemo {
	public static void main(String[] args) throws Exception {
		Subject subject = new RealSubject();
		InvocationHandler logHandler = new LogHandler(subject);
		Object proxyObject = Proxy.newProxyInstance(logHandler.getClass()
		        .getClassLoader(), subject.getClass().getInterfaces(),
		        logHandler);
		System.out.println("proxyObject -> " + proxyObject.getClass());
		Subject proxySubject = (Subject) proxyObject;
		proxySubject.rent();
		// proxySubject.say("sam");

	}
}

class LogHandler implements InvocationHandler {
	private Object subject;

	public LogHandler(Object subject) {
		this.subject = subject;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
	        throws Throwable {
		System.out.println("params proxy -> " + proxy.getClass());
		// 这里的proxy就是代理类对象，这里貌似没用，handler里面有proxy引用，proxy里面也有handler引用
		System.out.println("before");
		method.invoke(subject, args);
		System.out.println("after");
		return null;
	}

}

class RealSubject implements Subject {
	@Override
	public void rent() {
		System.out.println("i want rent my house");
	}

	@Override
	public void say(String name) {
		System.out.println("hello, " + name);
	}
}

interface Subject {
	public void rent();

	public void say(String name);
}