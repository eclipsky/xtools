package org.sky.x.reflect;

import java.io.Serializable;
import java.util.ArrayList;

public class GenericDemo {
    public static void main(String[] args) {
        Box<String> box = new Box<>();
        Box rawBox = new Box();
        box = rawBox;
        Serializable s = pick("d", new ArrayList<>());
    }

    static <T> T pick(T a1, T a2){
        return a2;
    }
}

class Box<T>{
    public T get(){
        return null;
    }
}
