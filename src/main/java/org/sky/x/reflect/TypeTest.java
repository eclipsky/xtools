package org.sky.x.reflect;


import java.lang.reflect.*;
import java.util.List;

public class TypeTest {
    public static void main(String[] args) throws NoSuchFieldException {
        test();
        Mathine<String> str = new Mathine<String>("hello"){};
        print(str);
    }


    private static void test(){
        Integer a = 1;
        Integer b = 2;
        Integer c = 3;
        Integer d = 3;
        Integer e = 321;
        Integer f = 321;
        Long g = 3L;
        System.out.println(c == d);
        System.out.println(e == f);
        System.out.println(c == (a + b));
        System.out.println(c.equals(a + b));
        System.out.println(g == (a + b));
        System.out.println(g.equals(a + b));
    }
//    private static String method(List<Integer> list){
//        return null;
//    }
//
//    private static int method(List<String> list){
//        return 0;
//    }

    public static void print(Mathine<String> obj) throws NoSuchFieldException {
        Type typeMathine = obj.getClass().getGenericSuperclass();
        System.out.println(typeMathine);
        Type typeData = obj.getData().getClass().getGenericSuperclass();
        System.out.println(typeData);
//        Field[] declaredFields = obj.getClass().getSuperclass().getDeclaredFields();
//        for (int i = 0; i < declaredFields.length; i++) {
//            Field declaredField = declaredFields[i];
//            Type genericType = declaredField.getGenericType();
//            System.out.println("变量名：" + declaredField.getName());
//            System.out.println("变量声明的类型："+genericType.getTypeName());
//            if (genericType instanceof Class) {
//                System.out.println("变量类型为Class");
//            } else if (genericType instanceof ParameterizedType) {
//                System.out.println("变量类型为ParameterizedType -> " + ((ParameterizedType)genericType).getActualTypeArguments()[0].getTypeName());
//            } else if (genericType instanceof TypeVariable) {
//                System.out.println("变量类型为TypeVariable -> " + ((TypeVariable)genericType).getTypeName());
//            } else if (genericType instanceof GenericArrayType) {
//                System.out.println("变量类型为GenericArrayType");
//            } else if (genericType instanceof WildcardType) {
//                System.out.println("变量类型为WildcardType");
//            }
//            System.out.println();
//        }
    }
}

class Mathine<T>{
    public T data;
    public List<T> list;
    Mathine(T t){
        this.data = t;
    }
    public void print(){
        System.out.println(data.toString());
    }

    public T getData() {
        return data;
    }

    public List<T> getList() {
        return list;
    }
}
