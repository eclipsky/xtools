package org.sky.x.reflect;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TypeDemo {

	public static void main(String[] args) {
		TypeDemo demo = new TypeDemo();
		demo.baseType();
		System.out.println(demo.getObject("1243"));
	}

	public void baseType() {
		Type type = String.class;
		System.out.println(type);

		Class clazz = String.class;
		System.out.println(clazz);

		type = int.class;
		String[] arr = new String[] {};
		clazz = arr.getClass();
		System.out.println(type);
		System.out.println(clazz);
	}

	public <T> String getObject(T t) {
		Type type = t.getClass();
		return type.toString();
	}

	public List<? extends Fruit> getFruitList() {
		List<? extends Fruit> fList = new ArrayList<Apple>();
		fList.add(null);
		return fList;
	}
}

class Food {
}

class Fruit extends Food {
}

class Apple extends Fruit {
}
