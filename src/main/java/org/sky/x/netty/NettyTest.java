package org.sky.x.netty;

import java.io.PrintWriter;
import java.io.StringWriter;

public class NettyTest {

    public static void main(String[] args) {
        int i = 0;
        try{
            int result = 100/i;
        }catch (Exception e){
//            e.printStackTrace();
            System.out.println(e.getMessage());
            String msg = printStackTrace(e);
            System.out.println(msg);
        }
    }

    public static String printStackTrace(Throwable e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String result = sw.toString();
        return result;
    }
}
