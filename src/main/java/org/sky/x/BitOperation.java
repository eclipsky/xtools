package org.sky.x;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre/>
 * 位运算
 * @author sam
 * 关于原码，补码，反码：@see http://www.cnblogs.com/zhangziqiu/archive/2011/03/30/ComputerCode.html
 * [-128 ~ 127]
 * 14 - 15
 * 		原码		   反码		  补码
 * 14 > 00001110 > 00001110 > 00001110
 * 15 > 10001111 > 11110000 > 11110001
 * -1 < 10000001 < 11111110 < 11111111
 * 
 * 14 - 13
 *      原码        反码        补码
 * 14 > 00001110 > 00001110 > 00001110
 * 13 > 10001101 > 11110010 > 11110011
 * +1 < 00000001 < 00000001 < 00000001
 * 
 * 取余 vs 取模：@see https://blog.csdn.net/coder_panyy/article/details/73743722
 * 
 *
 */
public class BitOperation {
	public static void main(String[] args) {
		// System.out.println((-19) % 12);
		// System.out.println((19) % 12);
//		bitOper();
		System.out.println(0 << 29);
		Map<String, String> map = new HashMap<String, String>(23);
		map.put("sam", "sara");
		String key = "sam";
		System.out.println(Integer.MAX_VALUE);
		int n = 0xD1A24C9F; // 10000001
		int hash = n  ^ (n >>> 16);
		n |= n >>> 1; // 01000001 -> 11000001
        n |= n >>> 2; // 00111110 -> 11111111
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
	}

	public static void bitOper() {

		// 左移位，右边统一补0，右移位，左边补符号位
		int i = -1000001093;
		System.out.println(i + "左移0位 > " + Integer.toBinaryString(i << 0));
		System.out.println(i + "左移3位 > " + Integer.toBinaryString(i << 3));
		System.out.println(i + "右移3位 > " + Integer.toBinaryString(i >> 3));
		System.out.println(i);
		System.out.println("");

		i = 1000001093;
		System.out
		        .println("+" + i + "左移0位 > " + Integer.toBinaryString(i << 0));
		System.out
		        .println("+" + i + "左移3位 > " + Integer.toBinaryString(i << 3));
		System.out
		        .println("+" + i + "右移3位 > " + Integer.toBinaryString(i >> 3));
		System.out.println("+" + i);
	}
}
