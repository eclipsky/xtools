package org.sky.x.gc;

public class Finalizer {

    public static void main(String[] args) {
        Finalizer obj = new Finalizer();
//        obj = null;
        System.gc();
//        System.runFinalization();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Finalizer#finalize()");
    }
}
