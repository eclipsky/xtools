package org.sky.x.cache;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class CaffeineCache {
    @Test
    public void manual(){
        int key = 18;
        String name = "sara";
        Cache<Integer, String> cache = Caffeine.newBuilder().expireAfterWrite(10, TimeUnit.SECONDS)
                .maximumSize(10_000).build();
        cache.put(key, name);
        name = cache.getIfPresent(key);
        name = cache.get(key, k -> "str:" + key);
        System.out.println(cache.getIfPresent(key));
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(cache.getIfPresent(key));
        cache.invalidate(key);
    }

    @Test
    public void Loading(){
        LoadingCache<Integer, String> cache = Caffeine.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).
                maximumSize(10_100).build(key -> "str:" + key);
        int key = 18;
        String val = cache.get(key);
        System.out.println(cache.get(key));
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(cache.get(key));
        Map<Integer, String> kids = cache.getAll(Arrays.asList(key));
        cache.policy().expireAfterAccess();

    }

    @Test
    public void Asynchronous(){
        AsyncLoadingCache<Integer, String> cache = Caffeine.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).
                maximumSize(10_100).buildAsync(key -> "str:" + key);
        int key = 18;
        CompletableFuture<String> future = cache.get(key);
    }

}
