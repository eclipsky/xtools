package org.sky.x.lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

/**
 * http://www.importnew.com/16436.html
 * 
 * <pre/>
 * @author sam.xie
 *
 */
public class LambdaDemo {

	public static void main(String[] args) {
		LambdaDemo demo = new LambdaDemo();
		System.out.println("sam");
		demo.test1();
	}

	// @Test
	public void test1() {
		// new Thread(new Runnable() {
		// @Override
		// public void run() {
		// // TODO Auto-generated method stub
		// System.out
		// .println("Before Java8, too much code for too little to do");
		// }
		// }).start();

		// new Thread(() ->
		// System.out.println("After Java8, Lambda is good")).start();
		Runnable runnable = () -> System.out.println(""); 
		new Thread(
				() -> System.out
						.println("In Java8, Lambda expression rocks !!"))
				.start();
	}

	// @Test
	public void test2() {
		List<String> names = Arrays.asList("sam", "sara");
		names.forEach(n -> System.out.println(n));

		names.forEach(System.out::println);
	}

	// @Test
	public void test3() {
		List<String> languages = Arrays.asList("C", "Java", "C++", "Ruby",
				"Python");
		System.out.println(">>>>> Launguages which start wich J");
		filter(languages, str -> str.startsWith("J"));

		System.out.println(">>>>> Launguages which end wich y");
		filter(languages, str -> str.endsWith("y"));

		System.out.println(">>>>> Launguages all");
		filter(languages, str -> true);

		System.out.println(">>>>> Launguages none");
		filter(languages, str -> false);
	}

	// @Test
	public void test4() {
		List<String> languages = Arrays.asList("C", "Java", "C++", "Ruby",
				"Python");
		languages.forEach(a -> System.out.print(a + "\t"));
		Collections.sort(languages, (o1, o2) -> o1.compareTo(o2));
		System.out.println();
		languages.forEach(a -> System.out.print(a + "\t"));
		System.out.println();
		languages.stream().map(a -> {
			return a.toLowerCase();
		}).collect(Collectors.toList())
				.forEach(a -> System.out.print(a + "\t"));
	}

//	@Test
	public void test5() {
		List<String> names = Arrays.asList("sam.xie", "sara.ouyang",
				"love.forever");
		// List<Integer> ids = Arrays.asList(12, 34, 56);
		// names.stream().filter(a -> a.startsWith("s"))
		// .forEach(a -> System.out.println(a + "\t"));
		// names.stream().map(a ->
		// a.toUpperCase()).forEach(System.out::println);
		Stream<String> words = names.stream().flatMap(
				a -> Stream.of(a.split("\\.")));
		words.forEach(w -> System.out.println(w));
	}

	@Test
	public void test6() {
		List<Integer> nums = Arrays.asList(1, 2, null, 3, 4, 6, null, 8, 9);
		int total = nums.stream().filter(num -> num != null).distinct()
				.mapToInt(num -> num * num).peek(System.out::println).skip(2)
				.limit(4).sum(); // 这里并没有打印9*9=81这个数，why？ 
		System.out.println(total);
	}

	private void filter(List<String> names, Predicate<String> condition) {
		for (String name : names) {
			if (condition.test(name)) {
				System.out.println(name);
			}
		}
	}

}
