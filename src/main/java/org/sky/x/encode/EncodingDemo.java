package org.sky.x.encode;

import java.io.UnsupportedEncodingException;

public class EncodingDemo {
	public static void main(String[] args) {
		EncodingDemo demo = new EncodingDemo();
		demo.testEncoding();
		demo.testChar();
		System.out.println("\u003f"); // 中文乱码3f
	}

	public void testChar() {
		char a = 'a';
		char b = '啊';
		System.out.println(Integer.toHexString(a));
		System.out.println(Integer.toHexString(b));
	}

	public void testEncoding() {
		// utf-16会带有两字节的BOM标识
		String[] charsetNames = { "utf-8", "unicode", "utf-16", "UTF-16BE",
				"UTF-16LE", "UTF-32", "UTF-32BE", "UTF-32LE", "GBK", "GB2312",
				"GB18030", "ISO8859-1", "BIG5", "ASCII" };

		for (int i = 0; i < charsetNames.length; i++) {
			printByteLength(charsetNames[i]);
		}
	}

	public static String getCode(String content, String format)
			throws UnsupportedEncodingException {
		byte[] bytes = content.getBytes(format);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toHexString(bytes[i] & 0xff).toUpperCase() + " ");
			// sb.append(Integer.toBinaryString(bytes[i] & 0xff).toUpperCase() +
			// " ");
		}
		sb.append(", " + Integer.toHexString(content.codePointAt(0)).toUpperCase()); // 打印指定索引字符的unicode point

		return sb.toString();
	}

	/**
	 * String类的不带参数的getBytes()方法会以程序所运行平台的默认编码方式为准来进行转换，
	 * 在不同环境下可能会有不同的结果，因此建议使用指定编码方式的getBytes(String charsetName)方法。
	 * unicode与utf-16的编码完全一致，包含FE FF表示大端格式
	 */
	public static void printByteLength(String charsetName) {
		String a = "a"; // 一个英文字符
		String b = "啊"; // 一个中文字符
		try {
			System.out.println(charsetName + "编码英文字符所占字节数:"
					+ a.getBytes(charsetName).length + ", "
					+ getCode(a, charsetName));
			System.out.println(charsetName + "编码中文字符所占字节数:"
					+ b.getBytes(charsetName).length + ", "
					+ getCode(b, charsetName));
			System.out.println();
		} catch (UnsupportedEncodingException e) {
			System.out.println("非法编码格式！");
		}
	}
}
