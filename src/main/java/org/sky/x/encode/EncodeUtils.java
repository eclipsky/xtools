package org.sky.x.encode;

public class EncodeUtils {

	private final static char[] BASE64_ARRAY = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
			'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
			'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3',
			'4', '5', '6', '7', '8', '9', '+', '/' };
	private final static char PLACE_HOLDER = '=';

	public static char[] base64Encode(String origin) {
		byte[] bytes = origin.getBytes();
		int triple = (bytes.length - 1) / 3 + 1;
		char[] encodeChars = new char[triple * 4];
		int num = bytes.length % 3;
		int originOffset = 0;
		int encodeOffset = 0;
		for (int i = 0; i < triple - 1 + (num == 0 ? 1 : 0); i++) {
			// 每三个字节作为一个处理单元，8*3=6*4
			originOffset = i * 3;
			encodeOffset = i * 4;
			byte origin_0 = bytes[originOffset + 0];
			byte origin_1 = bytes[originOffset + 1];
			byte origin_2 = bytes[originOffset + 2];
			encodeChars[encodeOffset + 0] = BASE64_ARRAY[(origin_0 >> 2) & 0xff];
			encodeChars[encodeOffset + 1] = BASE64_ARRAY[((origin_0 << 4) | (origin_1 >> 4)) & 0x3f];
			encodeChars[encodeOffset + 2] = BASE64_ARRAY[((origin_1 << 2) | (origin_2 >> 6)) & 0x3f];
			encodeChars[encodeOffset + 3] = BASE64_ARRAY[origin_2 & 0x3f];
			originOffset += 3;
			encodeOffset += 4;
		}
		// 处理最后多余字节
		if (num == 1) {
			encodeChars[encodeOffset + 0] = BASE64_ARRAY[(bytes[originOffset + 0] >> 2) & 0xff];
			encodeChars[encodeOffset + 1] = BASE64_ARRAY[(bytes[originOffset + 0] << 4) & 0xff];
			encodeChars[encodeOffset + 2] = PLACE_HOLDER;
			encodeChars[encodeOffset + 3] = PLACE_HOLDER;
		} else if (num == 2) {
			// 位运算优先级 | 小于 &
			encodeChars[encodeOffset + 0] = BASE64_ARRAY[(bytes[originOffset + 0] >> 2) & 0xff];
			encodeChars[encodeOffset + 1] = BASE64_ARRAY[((bytes[originOffset + 0] << 4) | (bytes[originOffset + 1] >> 4)) & 0xff];
			encodeChars[encodeOffset + 2] = BASE64_ARRAY[(bytes[originOffset + 1] << 2) & 0x3f];
			encodeChars[encodeOffset + 3] = PLACE_HOLDER;
		}

		return encodeChars;
	}

	public static String base64Decode(String code) {
		return null;
	}

	public static void main(String[] args) {
		String src = "BdddC";
		// System.out.println(i >> 1);
		// System.out.println(i);
		// System.out.println(-4 / 3);
		System.out.println(String.valueOf(base64Encode(src)));
		System.out.println((66 << 4 | 67 >> 4) & 0xff);
		System.out.println(66 << 4 | 67 >> 4 & 0xff);
		System.out.println(66 << 4 | (67 >> (4 & 0xff))); // 位运算优先级，&大于移位运算
		// System.out.println(77 << 2);
	}
}
